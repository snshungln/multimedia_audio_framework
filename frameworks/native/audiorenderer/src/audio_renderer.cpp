/*
 * Copyright (c) 2021-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef LOG_TAG
#define LOG_TAG "AudioRenderer"
#endif

#include <sstream>
#include "securec.h"
#include <atomic>
#include <cinttypes>
#include <memory>

#include "audio_renderer.h"
#include "audio_renderer_private.h"

#include "audio_renderer_log.h"
#include "audio_errors.h"
#include "audio_policy_manager.h"

#include "media_monitor_manager.h"

namespace OHOS {
namespace AudioStandard {

static const std::vector<StreamUsage> NEED_VERIFY_PERMISSION_STREAMS = {
    STREAM_USAGE_SYSTEM,
    STREAM_USAGE_DTMF,
    STREAM_USAGE_ENFORCED_TONE,
    STREAM_USAGE_ULTRASONIC,
    STREAM_USAGE_VOICE_MODEM_COMMUNICATION
};
static constexpr uid_t UID_MSDP_SA = 6699;
static constexpr int32_t WRITE_UNDERRUN_NUM = 100;
static constexpr int32_t MINIMUM_BUFFER_SIZE_MSEC = 5;
static constexpr int32_t MAXIMUM_BUFFER_SIZE_MSEC = 20;
constexpr int32_t TIME_OUT_SECONDS = 10;
constexpr int32_t START_TIME_OUT_SECONDS = 15;
static const std::map<AudioStreamType, StreamUsage> STREAM_TYPE_USAGE_MAP = {
    {STREAM_MUSIC, STREAM_USAGE_MUSIC},
    {STREAM_VOICE_CALL, STREAM_USAGE_VOICE_COMMUNICATION},
    {STREAM_VOICE_CALL_ASSISTANT, STREAM_USAGE_VOICE_CALL_ASSISTANT},
    {STREAM_VOICE_ASSISTANT, STREAM_USAGE_VOICE_ASSISTANT},
    {STREAM_ALARM, STREAM_USAGE_ALARM},
    {STREAM_VOICE_MESSAGE, STREAM_USAGE_VOICE_MESSAGE},
    {STREAM_RING, STREAM_USAGE_RINGTONE},
    {STREAM_NOTIFICATION, STREAM_USAGE_NOTIFICATION},
    {STREAM_ACCESSIBILITY, STREAM_USAGE_ACCESSIBILITY},
    {STREAM_SYSTEM, STREAM_USAGE_SYSTEM},
    {STREAM_MOVIE, STREAM_USAGE_MOVIE},
    {STREAM_GAME, STREAM_USAGE_GAME},
    {STREAM_SPEECH, STREAM_USAGE_AUDIOBOOK},
    {STREAM_NAVIGATION, STREAM_USAGE_NAVIGATION},
    {STREAM_DTMF, STREAM_USAGE_DTMF},
    {STREAM_SYSTEM_ENFORCED, STREAM_USAGE_ENFORCED_TONE},
    {STREAM_ULTRASONIC, STREAM_USAGE_ULTRASONIC},
    {STREAM_VOICE_RING, STREAM_USAGE_VOICE_RINGTONE},
};

static const std::vector<StreamUsage> AUDIO_DEFAULT_OUTPUT_DEVICE_SUPPORTED_STREAM_USAGES {
    STREAM_USAGE_VOICE_COMMUNICATION,
    STREAM_USAGE_VOICE_MESSAGE,
    STREAM_USAGE_VIDEO_COMMUNICATION,
    STREAM_USAGE_VOICE_MODEM_COMMUNICATION,
};

static AudioRendererParams SetStreamInfoToParams(const AudioStreamInfo &streamInfo)
{
    AudioRendererParams params;
    params.sampleFormat = streamInfo.format;
    params.sampleRate = streamInfo.samplingRate;
    params.channelCount = streamInfo.channels;
    params.encodingType = streamInfo.encoding;
    params.channelLayout = streamInfo.channelLayout;
    return params;
}

std::mutex AudioRenderer::createRendererMutex_;

AudioRenderer::~AudioRenderer() = default;
AudioRendererPrivate::~AudioRendererPrivate()
{
    AUDIO_INFO_LOG("Destruct in");
    abortRestore_ = true;

    std::shared_ptr<OutputDeviceChangeWithInfoCallbackImpl> outputDeviceChangeCallback = outputDeviceChangeCallback_;
    if (outputDeviceChangeCallback != nullptr) {
        outputDeviceChangeCallback->RemoveCallback();
        outputDeviceChangeCallback->UnsetAudioRendererObj();
    }
    std::shared_ptr<AudioRendererConcurrencyCallbackImpl> cb = audioConcurrencyCallback_;
    if (cb != nullptr) {
        cb->UnsetAudioRendererObj();
        AudioPolicyManager::GetInstance().UnsetAudioConcurrencyCallback(sessionID_);
    }
    for (auto id : usedSessionId_) {
        AudioPolicyManager::GetInstance().UnregisterDeviceChangeWithInfoCallback(id);
    }

    RendererState state = GetStatus();
    if (state != RENDERER_RELEASED && state != RENDERER_NEW) {
        Release();
    }

    if (rendererProxyObj_ != nullptr) {
        rendererProxyObj_->UnsetRendererObj();
        AudioPolicyManager::GetInstance().RemoveClientTrackerStub(sessionID_);
    }

    RemoveRendererPolicyServiceDiedCallback();
    DumpFileUtil::CloseDumpFile(&dumpFile_);
}

int32_t AudioRenderer::CheckMaxRendererInstances()
{
    std::vector<std::shared_ptr<AudioRendererChangeInfo>> audioRendererChangeInfos;
    AudioPolicyManager::GetInstance().GetCurrentRendererChangeInfos(audioRendererChangeInfos);
    AUDIO_INFO_LOG("Audio current renderer change infos size: %{public}zu", audioRendererChangeInfos.size());
    int32_t maxRendererInstances = AudioPolicyManager::GetInstance().GetMaxRendererInstances();
    if (audioRendererChangeInfos.size() >= static_cast<size_t>(maxRendererInstances)) {
        std::map<int32_t, int32_t> appUseNumMap;
        int32_t INITIAL_VALUE = 1;
        int32_t mostAppUid = -1;
        int32_t mostAppNum = -1;
        for (auto it = audioRendererChangeInfos.begin(); it != audioRendererChangeInfos.end(); it++) {
            auto appUseNum = appUseNumMap.find((*it)->clientUID);
            if (appUseNum != appUseNumMap.end()) {
                appUseNumMap[(*it)->clientUID] = ++appUseNum->second;
            } else {
                appUseNumMap.emplace((*it)->clientUID, INITIAL_VALUE);
            }
        }
        for (auto iter = appUseNumMap.begin(); iter != appUseNumMap.end(); iter++) {
            if (iter->second > mostAppNum) {
                mostAppNum = iter->second;
                mostAppUid = iter->first;
            }
        }
        std::shared_ptr<Media::MediaMonitor::EventBean> bean = std::make_shared<Media::MediaMonitor::EventBean>(
            Media::MediaMonitor::ModuleId::AUDIO, Media::MediaMonitor::EventId::AUDIO_STREAM_EXHAUSTED_STATS,
            Media::MediaMonitor::EventType::FREQUENCY_AGGREGATION_EVENT);
        bean->Add("CLIENT_UID", mostAppUid);
        bean->Add("TIMES", mostAppNum);
        Media::MediaMonitor::MediaMonitorManager::GetInstance().WriteLogMsg(bean);
    }

    CHECK_AND_RETURN_RET_LOG(audioRendererChangeInfos.size() < static_cast<size_t>(maxRendererInstances), ERR_OVERFLOW,
        "The current number of audio renderer streams is greater than the maximum number of configured instances");

    return SUCCESS;
}

size_t GetFormatSize(const AudioStreamParams& info)
{
    size_t bitWidthSize = 0;
    switch (info.format) {
        case SAMPLE_U8:
            bitWidthSize = 1; // size is 1
            break;
        case SAMPLE_S16LE:
            bitWidthSize = 2; // size is 2
            break;
        case SAMPLE_S24LE:
            bitWidthSize = 3; // size is 3
            break;
        case SAMPLE_S32LE:
            bitWidthSize = 4; // size is 4
            break;
        default:
            bitWidthSize = 2; // size is 2
            break;
    }
    return bitWidthSize;
}

std::unique_ptr<AudioRenderer> AudioRenderer::Create(AudioStreamType audioStreamType)
{
    AppInfo appInfo = {};
    return Create(audioStreamType, appInfo);
}

std::unique_ptr<AudioRenderer> AudioRenderer::Create(AudioStreamType audioStreamType, const AppInfo &appInfo)
{
    if (audioStreamType == STREAM_MEDIA) {
        audioStreamType = STREAM_MUSIC;
    }

    return std::make_unique<AudioRendererPrivate>(audioStreamType, appInfo, true);
}

std::unique_ptr<AudioRenderer> AudioRenderer::Create(const AudioRendererOptions &rendererOptions)
{
    AppInfo appInfo = {};
    return Create("", rendererOptions, appInfo);
}

std::unique_ptr<AudioRenderer> AudioRenderer::Create(const AudioRendererOptions &rendererOptions,
    const AppInfo &appInfo)
{
    return Create("", rendererOptions, appInfo);
}

std::unique_ptr<AudioRenderer> AudioRenderer::Create(const std::string cachePath,
    const AudioRendererOptions &rendererOptions)
{
    AppInfo appInfo = {};
    return Create(cachePath, rendererOptions, appInfo);
}

std::shared_ptr<AudioRenderer> AudioRenderer::CreateRenderer(const AudioRendererOptions &rendererOptions,
    const AppInfo &appInfo)
{
    auto tempUniquePtr = Create("", rendererOptions, appInfo);
    std::shared_ptr<AudioRenderer> sharedPtr(tempUniquePtr.release());
    return sharedPtr;
}

std::unique_ptr<AudioRenderer> AudioRenderer::Create(const std::string cachePath,
    const AudioRendererOptions &rendererOptions, const AppInfo &appInfo)
{
    Trace trace("AudioRenderer::Create");
    std::lock_guard<std::mutex> lock(createRendererMutex_);
    CHECK_AND_RETURN_RET_LOG(AudioPolicyManager::GetInstance().GetAudioPolicyManagerProxy() != nullptr,
        nullptr, "sa not start");
    AudioStreamType audioStreamType = IAudioStream::GetStreamType(rendererOptions.rendererInfo.contentType,
        rendererOptions.rendererInfo.streamUsage);
    if (audioStreamType == STREAM_ULTRASONIC && getuid() != UID_MSDP_SA) {
        AudioRenderer::SendRendererCreateError(rendererOptions.rendererInfo.streamUsage,
            ERR_INVALID_PARAM);
        AUDIO_ERR_LOG("ULTRASONIC can only create by MSDP");
        return nullptr;
    }

    auto audioRenderer = std::make_unique<AudioRendererPrivate>(audioStreamType, appInfo, false);
    if (audioRenderer == nullptr) {
        AudioRenderer::SendRendererCreateError(rendererOptions.rendererInfo.streamUsage,
            ERR_OPERATION_FAILED);
    }
    CHECK_AND_RETURN_RET_LOG(audioRenderer != nullptr, nullptr, "Failed to create renderer object");

    int32_t rendererFlags = rendererOptions.rendererInfo.rendererFlags;
    AUDIO_INFO_LOG("StreamClientState for Renderer::Create. content: %{public}d, usage: %{public}d, "\
        "flags: %{public}d, uid: %{public}d", rendererOptions.rendererInfo.contentType,
        rendererOptions.rendererInfo.streamUsage, rendererFlags, appInfo.appUid);

    audioRenderer->rendererInfo_.contentType = rendererOptions.rendererInfo.contentType;
    audioRenderer->rendererInfo_.streamUsage = rendererOptions.rendererInfo.streamUsage;
    audioRenderer->rendererInfo_.isSatellite = rendererOptions.rendererInfo.isSatellite;
    audioRenderer->rendererInfo_.playerType = rendererOptions.rendererInfo.playerType;
    audioRenderer->rendererInfo_.expectedPlaybackDurationBytes
        = rendererOptions.rendererInfo.expectedPlaybackDurationBytes;
    audioRenderer->rendererInfo_.samplingRate = rendererOptions.streamInfo.samplingRate;
    audioRenderer->rendererInfo_.rendererFlags = rendererFlags;
    audioRenderer->rendererInfo_.originalFlag = rendererFlags;
    audioRenderer->privacyType_ = rendererOptions.privacyType;
    audioRenderer->strategy_ = rendererOptions.strategy;
    audioRenderer->originalStrategy_ = rendererOptions.strategy;
    AudioRendererParams params = SetStreamInfoToParams(rendererOptions.streamInfo);
    if (audioRenderer->SetParams(params) != SUCCESS) {
        AUDIO_ERR_LOG("SetParams failed in renderer");
        audioRenderer = nullptr;
        AudioRenderer::SendRendererCreateError(rendererOptions.rendererInfo.streamUsage,
            ERR_OPERATION_FAILED);
    }

    return audioRenderer;
}

void AudioRenderer::SendRendererCreateError(const StreamUsage &sreamUsage,
    const int32_t &errorCode)
{
    std::shared_ptr<Media::MediaMonitor::EventBean> bean = std::make_shared<Media::MediaMonitor::EventBean>(
        Media::MediaMonitor::AUDIO, Media::MediaMonitor::AUDIO_STREAM_CREATE_ERROR_STATS,
        Media::MediaMonitor::FREQUENCY_AGGREGATION_EVENT);
    bean->Add("IS_PLAYBACK", 1);
    bean->Add("CLIENT_UID", static_cast<int32_t>(getuid()));
    bean->Add("STREAM_TYPE", sreamUsage);
    bean->Add("ERROR_CODE", errorCode);
    Media::MediaMonitor::MediaMonitorManager::GetInstance().WriteLogMsg(bean);
}

AudioRendererPrivate::AudioRendererPrivate(AudioStreamType audioStreamType, const AppInfo &appInfo, bool createStream)
{
    appInfo_ = appInfo;
    if (!(appInfo_.appPid)) {
        appInfo_.appPid = getpid();
    }

    if (appInfo_.appUid < 0) {
        appInfo_.appUid = static_cast<int32_t>(getuid());
    }

    if (createStream) {
        AudioStreamParams tempParams = {};
        audioStream_ = IAudioStream::GetPlaybackStream(IAudioStream::PA_STREAM, tempParams, audioStreamType,
            appInfo_.appUid);
        if (audioStream_ && STREAM_TYPE_USAGE_MAP.count(audioStreamType) != 0) {
            // Initialize the streamUsage based on the streamType
            rendererInfo_.streamUsage = STREAM_TYPE_USAGE_MAP.at(audioStreamType);
        }
        AUDIO_INFO_LOG("AudioRendererPrivate create normal stream for old mode.");
    }

    rendererProxyObj_ = std::make_shared<AudioRendererProxyObj>();
    if (!rendererProxyObj_) {
        AUDIO_WARNING_LOG("AudioRendererProxyObj Memory Allocation Failed !!");
    }

    audioInterrupt_.audioFocusType.streamType = audioStreamType;
    audioInterrupt_.pid = appInfo_.appPid;
    audioInterrupt_.uid = appInfo_.appUid;
    audioInterrupt_.mode = SHARE_MODE;
    audioInterrupt_.parallelPlayFlag = false;

    state_ = RENDERER_PREPARED;
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
int32_t AudioRendererPrivate::InitAudioInterruptCallback()
{
    AUDIO_DEBUG_LOG("in");

    if (audioInterrupt_.streamId != 0) {
        AUDIO_INFO_LOG("old session already has interrupt, need to reset");
        (void)AudioPolicyManager::GetInstance().DeactivateAudioInterrupt(audioInterrupt_);
        (void)AudioPolicyManager::GetInstance().UnsetAudioInterruptCallback(audioInterrupt_.streamId);
    }

    CHECK_AND_RETURN_RET_LOG(audioInterrupt_.mode == SHARE_MODE || audioInterrupt_.mode == INDEPENDENT_MODE,
        ERR_INVALID_PARAM, "Invalid interrupt mode!");
    CHECK_AND_RETURN_RET_LOG(audioStream_->GetAudioSessionID(audioInterrupt_.streamId) == 0, ERR_INVALID_INDEX,
        "GetAudioSessionID failed");
    sessionID_ = audioInterrupt_.streamId;
    audioInterrupt_.streamUsage = rendererInfo_.streamUsage;
    audioInterrupt_.contentType = rendererInfo_.contentType;
    audioInterrupt_.sessionStrategy = strategy_;

    AUDIO_INFO_LOG("interruptMode %{public}d, streamType %{public}d, sessionID %{public}d",
        audioInterrupt_.mode, audioInterrupt_.audioFocusType.streamType, audioInterrupt_.streamId);

    if (audioInterruptCallback_ == nullptr) {
        audioInterruptCallback_ = std::make_shared<AudioRendererInterruptCallbackImpl>(audioStream_, audioInterrupt_);
        CHECK_AND_RETURN_RET_LOG(audioInterruptCallback_ != nullptr, ERROR,
            "Failed to allocate memory for audioInterruptCallback_");
    }
    return AudioPolicyManager::GetInstance().SetAudioInterruptCallback(sessionID_, audioInterruptCallback_,
        appInfo_.appUid);
}

int32_t AudioRendererPrivate::InitOutputDeviceChangeCallback()
{
    if (!outputDeviceChangeCallback_) {
        outputDeviceChangeCallback_ = std::make_shared<OutputDeviceChangeWithInfoCallbackImpl>();
        CHECK_AND_RETURN_RET_LOG(outputDeviceChangeCallback_ != nullptr, ERROR, "Memory allocation failed");
    }

    outputDeviceChangeCallback_->SetAudioRendererObj(this);

    uint32_t sessionId;
    int32_t ret = GetAudioStreamIdInner(sessionId);
    CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ret, "Get sessionId failed");

    usedSessionId_.push_back(sessionId);
    ret = AudioPolicyManager::GetInstance().RegisterDeviceChangeWithInfoCallback(sessionId,
        outputDeviceChangeCallback_);
    CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ret, "Register failed");

    return SUCCESS;
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
int32_t AudioRendererPrivate::InitAudioStream(AudioStreamParams audioStreamParams)
{
    Trace trace("AudioRenderer::InitAudioStream");
    AudioRenderer *renderer = this;
    rendererProxyObj_->SaveRendererObj(renderer);
    audioStream_->SetRendererInfo(rendererInfo_);
    audioStream_->SetClientID(appInfo_.appPid, appInfo_.appUid, appInfo_.appTokenId, appInfo_.appFullTokenId);

    SetAudioPrivacyTypeInner(privacyType_);
    audioStream_->SetStreamTrackerState(false);

    int32_t ret = audioStream_->SetAudioStreamInfo(audioStreamParams, rendererProxyObj_);
    CHECK_AND_RETURN_RET_LOG(!ret, ret, "SetParams SetAudioStreamInfo Failed");

    ret = GetAudioStreamIdInner(sessionID_);
    CHECK_AND_RETURN_RET_LOG(!ret, ret, "GetAudioStreamId err");
    InitLatencyMeasurement(audioStreamParams);
    InitAudioConcurrencyCallback();

    return SUCCESS;
}

int32_t AudioRendererPrivate::GetFrameCount(uint32_t &frameCount) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetFrameCount(frameCount);
}

int32_t AudioRendererPrivate::GetLatency(uint64_t &latency) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetLatency(latency);
}

void AudioRendererPrivate::SetAudioPrivacyType(AudioPrivacyType privacyType)
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_LOG(currentStream != nullptr, "audioStream_ is nullptr");
    privacyType_ = privacyType;
    currentStream->SetPrivacyType(privacyType);
}

AudioPrivacyType AudioRendererPrivate::GetAudioPrivacyType()
{
    return privacyType_;
}

IAudioStream::StreamClass AudioRendererPrivate::GetPreferredStreamClass(AudioStreamParams audioStreamParams)
{
    if (rendererInfo_.originalFlag == AUDIO_FLAG_FORCED_NORMAL) {
        return IAudioStream::PA_STREAM;
    }
    if (rendererInfo_.originalFlag == AUDIO_FLAG_MMAP &&
        !IAudioStream::IsStreamSupported(rendererInfo_.originalFlag, audioStreamParams)) {
        AUDIO_WARNING_LOG("Unsupported stream params, will create normal stream");
        rendererInfo_.originalFlag = AUDIO_FLAG_NORMAL;
        rendererInfo_.rendererFlags = AUDIO_FLAG_NORMAL;
    }

    int32_t flag = AudioPolicyManager::GetInstance().GetPreferredOutputStreamType(rendererInfo_);
    AUDIO_INFO_LOG("Preferred renderer flag: %{public}d", flag);
    if (flag == AUDIO_FLAG_MMAP) {
        rendererInfo_.rendererFlags = AUDIO_FLAG_MMAP;
        isFastRenderer_ = true;
        return IAudioStream::FAST_STREAM;
    }
    if (flag == AUDIO_FLAG_VOIP_FAST) {
        // It is not possible to directly create a fast VoIP stream
        isFastVoipSupported_ = true;
    } else if (flag == AUDIO_FLAG_VOIP_DIRECT) {
        isDirectVoipSupported_ = IsDirectVoipParams(audioStreamParams);
        rendererInfo_.originalFlag = isDirectVoipSupported_ ? AUDIO_FLAG_VOIP_DIRECT : AUDIO_FLAG_NORMAL;
        // The VoIP direct mode can only be used for RENDER_MODE_CALLBACK
        rendererInfo_.rendererFlags = (isDirectVoipSupported_ && audioRenderMode_ == RENDER_MODE_CALLBACK) ?
            AUDIO_FLAG_VOIP_DIRECT : AUDIO_FLAG_NORMAL;
        AUDIO_INFO_LOG("Preferred renderer flag is VOIP_DIRECT. Actual flag: %{public}d", rendererInfo_.rendererFlags);
        return IAudioStream::PA_STREAM;
    }

    AUDIO_INFO_LOG("Preferred renderer flag: AUDIO_FLAG_NORMAL");
    rendererInfo_.rendererFlags = AUDIO_FLAG_NORMAL;
    return IAudioStream::PA_STREAM;
}

bool AudioRendererPrivate::IsDirectVoipParams(const AudioStreamParams &audioStreamParams)
{
    // VoIP derect only supports 16K and 48K sampling rate.
    if (!(audioStreamParams.samplingRate == SAMPLE_RATE_16000 ||
        audioStreamParams.samplingRate == SAMPLE_RATE_48000)) {
        AUDIO_ERR_LOG("The sampling rate %{public}d is not supported for direct VoIP mode",
            audioStreamParams.samplingRate);
        return false;
    }

    // VoIP derect only supports MONO and STEREO.
    if (!(audioStreamParams.channels == MONO || audioStreamParams.channels == STEREO)) {
        AUDIO_ERR_LOG("The channels %{public}d is not supported for direct VoIP mode",
            audioStreamParams.channels);
        return false;
    }

    // VoIP derect only supports 16bit and 32bit.
    if (!(audioStreamParams.format == SAMPLE_S16LE || audioStreamParams.format == SAMPLE_S32LE)) {
        AUDIO_ERR_LOG("The format %{public}d is not supported for direct VoIP mode",
            audioStreamParams.format);
        return false;
    }

    AUDIO_INFO_LOG("Valid params for direct VoIP: sampling rate %{public}d, format %{public}d, channels %{public}d",
        audioStreamParams.samplingRate, audioStreamParams.format, audioStreamParams.channels);
    return true;
}

int32_t AudioRendererPrivate::SetParams(const AudioRendererParams params)
{
    Trace trace("AudioRenderer::SetParams");
    AUDIO_INFO_LOG("StreamClientState for Renderer::SetParams.");

    std::shared_lock<std::shared_mutex> lockShared(rendererMutex_);
    std::lock_guard<std::mutex> lock(setParamsMutex_);
    AudioStreamParams audioStreamParams = ConvertToAudioStreamParams(params);

    AudioStreamType audioStreamType = IAudioStream::GetStreamType(rendererInfo_.contentType, rendererInfo_.streamUsage);
#ifdef SUPPORT_LOW_LATENCY
    IAudioStream::StreamClass streamClass = GetPreferredStreamClass(audioStreamParams);
#else
    rendererInfo_.originalFlag = AUDIO_FLAG_FORCED_NORMAL;
    rendererInfo_.rendererFlags = AUDIO_FLAG_NORMAL;
    IAudioStream::StreamClass streamClass = IAudioStream::PA_STREAM;
#endif
    int32_t ret = PrepareAudioStream(audioStreamParams, audioStreamType, streamClass);
    CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ERR_INVALID_PARAM, "PrepareAudioStream failed");

    ret = InitAudioStream(audioStreamParams);
    // When the fast stream creation fails, a normal stream is created
    if (ret != SUCCESS && streamClass == IAudioStream::FAST_STREAM) {
        AUDIO_INFO_LOG("Create fast Stream fail, play by normal stream.");
        streamClass = IAudioStream::PA_STREAM;
        isFastRenderer_ = false;
        audioStream_ = IAudioStream::GetPlaybackStream(streamClass, audioStreamParams, audioStreamType,
            appInfo_.appUid);
        CHECK_AND_RETURN_RET_LOG(audioStream_ != nullptr,
            ERR_INVALID_PARAM, "SetParams GetPlayBackStream failed when create normal stream.");
        ret = InitAudioStream(audioStreamParams);
        CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ret, "InitAudioStream failed");
        audioStream_->SetRenderMode(RENDER_MODE_CALLBACK);
    }

    CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ret, "SetAudioStreamInfo Failed");
    AUDIO_INFO_LOG("SetAudioStreamInfo Succeeded");

    RegisterRendererPolicyServiceDiedCallback();
    // eg: 100005_44100_2_1_client_in.pcm
    std::string dumpFileName = std::to_string(sessionID_) + "_" + std::to_string(params.sampleRate) + "_" +
        std::to_string(params.channelCount) + "_" + std::to_string(params.sampleFormat) + "_client_in.pcm";
    DumpFileUtil::OpenDumpFile(DumpFileUtil::DUMP_CLIENT_PARA, dumpFileName, &dumpFile_);

    ret = InitOutputDeviceChangeCallback();
    CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ret, "InitOutputDeviceChangeCallback Failed");

    return InitAudioInterruptCallback();
}

int32_t AudioRendererPrivate::PrepareAudioStream(const AudioStreamParams &audioStreamParams,
    const AudioStreamType &audioStreamType, IAudioStream::StreamClass &streamClass)
{
    AUDIO_INFO_LOG("Create stream with flag: %{public}d, original flag: %{public}d, streamClass: %{public}d",
        rendererInfo_.rendererFlags, rendererInfo_.originalFlag, streamClass);

    // check AudioStreamParams for fast stream
    // As fast stream only support specified audio format, we should call GetPlaybackStream with audioStreamParams.
    ActivateAudioConcurrency(audioStreamParams, audioStreamType, streamClass);
    if (audioStream_ == nullptr) {
        audioStream_ = IAudioStream::GetPlaybackStream(streamClass, audioStreamParams, audioStreamType,
            appInfo_.appUid);
        CHECK_AND_RETURN_RET_LOG(audioStream_ != nullptr, ERR_INVALID_PARAM, "SetParams GetPlayBackStream failed.");
        AUDIO_INFO_LOG("IAudioStream::GetStream success");
        isFastRenderer_ = IAudioStream::IsFastStreamClass(streamClass);
    }
    return SUCCESS;
}

int32_t AudioRendererPrivate::GetParams(AudioRendererParams &params) const
{
    AudioStreamParams audioStreamParams;
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    int32_t result = currentStream->GetAudioStreamInfo(audioStreamParams);
    if (!result) {
        params.sampleFormat = static_cast<AudioSampleFormat>(audioStreamParams.format);
        params.sampleRate = static_cast<AudioSamplingRate>(audioStreamParams.samplingRate);
        params.channelCount = static_cast<AudioChannel>(audioStreamParams.channels);
        params.encodingType = static_cast<AudioEncodingType>(audioStreamParams.encoding);
        params.channelLayout = static_cast<AudioChannelLayout>(audioStreamParams.channelLayout);
    }

    return result;
}

int32_t AudioRendererPrivate::GetRendererInfo(AudioRendererInfo &rendererInfo) const
{
    rendererInfo = rendererInfo_;

    return SUCCESS;
}

int32_t AudioRendererPrivate::GetStreamInfo(AudioStreamInfo &streamInfo) const
{
    AudioStreamParams audioStreamParams;
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    int32_t result = currentStream->GetAudioStreamInfo(audioStreamParams);
    if (!result) {
        streamInfo.format = static_cast<AudioSampleFormat>(audioStreamParams.format);
        streamInfo.samplingRate = static_cast<AudioSamplingRate>(audioStreamParams.samplingRate);
        streamInfo.channels = static_cast<AudioChannel>(audioStreamParams.channels);
        streamInfo.encoding = static_cast<AudioEncodingType>(audioStreamParams.encoding);
        streamInfo.channelLayout = static_cast<AudioChannelLayout>(audioStreamParams.channelLayout);
    }

    return result;
}

int32_t AudioRendererPrivate::SetRendererCallback(const std::shared_ptr<AudioRendererCallback> &callback)
{
    std::shared_lock<std::shared_mutex> lockShared(rendererMutex_);
    std::lock_guard<std::mutex> lock(setStreamCallbackMutex_);
    // If the client is using the deprecated SetParams API. SetRendererCallback must be invoked, after SetParams.
    // In general, callbacks can only be set after the renderer state is PREPARED.
    RendererState state = GetStatusInner();
    CHECK_AND_RETURN_RET_LOG(state != RENDERER_NEW && state != RENDERER_RELEASED, ERR_ILLEGAL_STATE,
        "incorrect state:%{public}d to register cb", state);

    CHECK_AND_RETURN_RET_LOG(callback != nullptr, ERR_INVALID_PARAM,
        "callback param is null");

    // Save reference for interrupt callback
    CHECK_AND_RETURN_RET_LOG(audioInterruptCallback_ != nullptr, ERROR,
        "audioInterruptCallback_ == nullptr");
    std::shared_ptr<AudioRendererInterruptCallbackImpl> cbInterrupt =
        std::static_pointer_cast<AudioRendererInterruptCallbackImpl>(audioInterruptCallback_);
    cbInterrupt->SaveCallback(callback);

    // Save and Set reference for stream callback. Order is important here.
    if (audioStreamCallback_ == nullptr) {
        audioStreamCallback_ = std::make_shared<AudioStreamCallbackRenderer>();
        CHECK_AND_RETURN_RET_LOG(audioStreamCallback_ != nullptr, ERROR,
            "Failed to allocate memory for audioStreamCallback_");
    }
    std::shared_ptr<AudioStreamCallbackRenderer> cbStream =
        std::static_pointer_cast<AudioStreamCallbackRenderer>(audioStreamCallback_);
    cbStream->SaveCallback(callback);
    (void)audioStream_->SetStreamCallback(audioStreamCallback_);

    return SUCCESS;
}

int32_t AudioRendererPrivate::SetRendererPositionCallback(int64_t markPosition,
    const std::shared_ptr<RendererPositionCallback> &callback)
{
    CHECK_AND_RETURN_RET_LOG((callback != nullptr) && (markPosition > 0), ERR_INVALID_PARAM,
        "input param is invalid");
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    currentStream->SetRendererPositionCallback(markPosition, callback);

    return SUCCESS;
}

void AudioRendererPrivate::UnsetRendererPositionCallback()
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_LOG(currentStream != nullptr, "audioStream_ is nullptr");
    currentStream->UnsetRendererPositionCallback();
}

int32_t AudioRendererPrivate::SetRendererPeriodPositionCallback(int64_t frameNumber,
    const std::shared_ptr<RendererPeriodPositionCallback> &callback)
{
    CHECK_AND_RETURN_RET_LOG((callback != nullptr) && (frameNumber > 0), ERR_INVALID_PARAM,
        "input param is invalid");
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    currentStream->SetRendererPeriodPositionCallback(frameNumber, callback);

    return SUCCESS;
}

void AudioRendererPrivate::UnsetRendererPeriodPositionCallback()
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_LOG(currentStream != nullptr, "audioStream_ is nullptr");
    currentStream->UnsetRendererPeriodPositionCallback();
}

bool AudioRendererPrivate::IsAllowedStartBackgroud()
{
    bool ret = AudioPolicyManager::GetInstance().IsAllowedPlayback(appInfo_.appUid, appInfo_.appPid);
    if (ret) {
        AUDIO_INFO_LOG("AVSession IsAudioPlaybackAllowed is: %{public}d", ret);
        return ret;
    } else {
        if (std::count(BACKGROUND_NOSTART_STREAM_USAGE.begin(), BACKGROUND_NOSTART_STREAM_USAGE.end(),
            rendererInfo_.streamUsage) == 0) {
            AUDIO_INFO_LOG("%{public}d is BACKGROUND_NOSTART_STREAM_USAGE", rendererInfo_.streamUsage);
            return true;
        }
    }
    return ret;
}

// Must be called with rendererMutex_ or streamMutex_ held.
bool AudioRendererPrivate::GetStartStreamResult(StateChangeCmdType cmdType)
{
    bool result = audioStream_->StartAudioStream(cmdType);
    if (!result) {
        AUDIO_ERR_LOG("Start audio stream failed");
        std::lock_guard<std::mutex> lock(silentModeAndMixWithOthersMutex_);
        if (!audioStream_->GetSilentModeAndMixWithOthers()) {
            int32_t ret = AudioPolicyManager::GetInstance().DeactivateAudioInterrupt(audioInterrupt_);
            if (ret != 0) {
                AUDIO_WARNING_LOG("DeactivateAudioInterrupt Failed");
            }
        }
    }

    state_ = RENDERER_RUNNING;
    return result;
}

std::shared_ptr<IAudioStream> AudioRendererPrivate::GetInnerStream() const
{
    std::shared_lock<std::shared_mutex> lock(streamMutex_);
    return audioStream_;
}

bool AudioRendererPrivate::Start(StateChangeCmdType cmdType)
{
    Trace trace("AudioRenderer::Start");
    AudioXCollie audioXCollie("AudioRendererPrivate::Start", START_TIME_OUT_SECONDS,
        [](void *) {
            AUDIO_ERR_LOG("Start timeout");
        }, nullptr, AUDIO_XCOLLIE_FLAG_LOG | AUDIO_XCOLLIE_FLAG_RECOVERY);
    std::lock_guard<std::shared_mutex> lock(rendererMutex_);
    AUDIO_INFO_LOG("StreamClientState for Renderer::Start. id: %{public}u, streamType: %{public}d, "\
        "volume: %{public}f, interruptMode: %{public}d", sessionID_, audioInterrupt_.audioFocusType.streamType,
        GetVolumeInner(), audioInterrupt_.mode);
    CHECK_AND_RETURN_RET_LOG(IsAllowedStartBackgroud(), false, "Start failed. IsAllowedStartBackgroud is false");
    RendererState state = GetStatusInner();
    CHECK_AND_RETURN_RET_LOG((state == RENDERER_PREPARED) || (state == RENDERER_STOPPED) || (state == RENDERER_PAUSED),
        false, "Start failed. Illegal state:%{public}u", state);

    CHECK_AND_RETURN_RET_LOG(!isSwitching_, false,
        "Start failed. Switching state: %{public}d", isSwitching_);

    if (audioInterrupt_.audioFocusType.streamType == STREAM_DEFAULT ||
        audioInterrupt_.streamId == INVALID_SESSION_ID) {
        return false;
    }

    CHECK_AND_RETURN_RET_LOG(audioStream_ != nullptr, false, "audio stream is null");

    if (GetVolumeInner() == 0 && isStillMuted_) {
        AUDIO_INFO_LOG("StreamClientState for Renderer::Start. volume=%{public}f, isStillMuted_=%{public}d",
            GetVolumeInner(), isStillMuted_);
        audioInterrupt_.sessionStrategy.concurrencyMode = AudioConcurrencyMode::SILENT;
    } else {
        isStillMuted_ = false;
    }

    {
        std::lock_guard<std::mutex> lockSilentMode(silentModeAndMixWithOthersMutex_);
        if (audioStream_->GetSilentModeAndMixWithOthers()) {
            audioInterrupt_.sessionStrategy.concurrencyMode = AudioConcurrencyMode::SILENT;
        }
        int32_t ret = AudioPolicyManager::GetInstance().ActivateAudioInterrupt(audioInterrupt_);
        CHECK_AND_RETURN_RET_LOG(ret == 0, false, "ActivateAudioInterrupt Failed");
    }

    if (IsNoStreamRenderer()) {
        // no stream renderer only need to activate audio interrupt
        state_ = RENDERER_RUNNING;
        return true;
    }

    return GetStartStreamResult(cmdType);
}

int32_t AudioRendererPrivate::Write(uint8_t *buffer, size_t bufferSize)
{
    Trace trace("AudioRenderer::Write");
    MockPcmData(buffer, bufferSize);
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    int32_t size = currentStream->Write(buffer, bufferSize);
    if (size > 0) {
        DumpFileUtil::WriteDumpFile(dumpFile_, static_cast<void *>(buffer), size);
    }
    return size;
}

int32_t AudioRendererPrivate::Write(uint8_t *pcmBuffer, size_t pcmSize, uint8_t *metaBuffer, size_t metaSize)
{
    Trace trace("Write");
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    int32_t size = currentStream->Write(pcmBuffer, pcmSize, metaBuffer, metaSize);
    if (size > 0) {
        DumpFileUtil::WriteDumpFile(dumpFile_, static_cast<void *>(pcmBuffer), size);
    }
    return size;
}

RendererState AudioRendererPrivate::GetStatus() const
{
    if (IsNoStreamRenderer()) {
        return state_;
    }
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, RENDERER_INVALID, "audioStream_ is nullptr");
    return static_cast<RendererState>(currentStream->GetState());
}

bool AudioRendererPrivate::GetAudioTime(Timestamp &timestamp, Timestamp::Timestampbase base) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetAudioTime(timestamp, base);
}

bool AudioRendererPrivate::GetAudioPosition(Timestamp &timestamp, Timestamp::Timestampbase base) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetAudioPosition(timestamp, base);
}

bool AudioRendererPrivate::Drain() const
{
    Trace trace("AudioRenderer::Drain");
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->DrainAudioStream();
}

bool AudioRendererPrivate::Flush() const
{
    Trace trace("AudioRenderer::Flush");
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->FlushAudioStream();
}

bool AudioRendererPrivate::PauseTransitent(StateChangeCmdType cmdType)
{
    Trace trace("AudioRenderer::PauseTransitent");
    std::lock_guard<std::shared_mutex> lock(rendererMutex_);
    AUDIO_INFO_LOG("StreamClientState for Renderer::PauseTransitent. id: %{public}u", sessionID_);
    if (isSwitching_) {
        AUDIO_ERR_LOG("failed. Switching state: %{public}d", isSwitching_);
        return false;
    }

    if (IsNoStreamRenderer()) {
        // no stream renderer don't need to change audio stream state
        state_ = RENDERER_PAUSED;
        return true;
    }

    RendererState state = GetStatusInner();
    if (state != RENDERER_RUNNING) {
        // If the stream is not running, there is no need to pause and deactive audio interrupt
        AUDIO_ERR_LOG("State of stream is not running. Illegal state:%{public}u", state);
        return false;
    }
    bool result = audioStream_->PauseAudioStream(cmdType);
    if (result) {
        state_ = RENDERER_PAUSED;
    }

    return result;
}

bool AudioRendererPrivate::Mute(StateChangeCmdType cmdType) const
{
    Trace trace("AudioRenderer::Mute");
    std::shared_lock<std::shared_mutex> lock(rendererMutex_);

    AUDIO_INFO_LOG("StreamClientState for Renderer::Mute. id: %{public}u", sessionID_);
    (void)audioStream_->SetMute(true);
    return true;
}

bool AudioRendererPrivate::Unmute(StateChangeCmdType cmdType) const
{
    Trace trace("AudioRenderer::Unmute");
    std::shared_lock<std::shared_mutex> lock(rendererMutex_);

    AUDIO_INFO_LOG("StreamClientState for Renderer::Unmute. id: %{public}u", sessionID_);
    (void)audioStream_->SetMute(false);
    return true;
}

bool AudioRendererPrivate::Pause(StateChangeCmdType cmdType)
{
    Trace trace("AudioRenderer::Pause");
    AudioXCollie audioXCollie("AudioRenderer::Pause", TIME_OUT_SECONDS);
    std::lock_guard<std::shared_mutex> lock(rendererMutex_);

    AUDIO_INFO_LOG("StreamClientState for Renderer::Pause. id: %{public}u", sessionID_);

    CHECK_AND_RETURN_RET_LOG(!isSwitching_, false, "Pause failed. Switching state: %{public}d", isSwitching_);

    if (IsNoStreamRenderer()) {
        // When the cellular call stream is pausing, only need to deactivate audio interrupt.
        if (AudioPolicyManager::GetInstance().DeactivateAudioInterrupt(audioInterrupt_) != 0) {
            AUDIO_ERR_LOG("DeactivateAudioInterrupt Failed");
        }
        state_ = RENDERER_PAUSED;
        return true;
    }

    RendererState state = GetStatusInner();
    CHECK_AND_RETURN_RET_LOG(state == RENDERER_RUNNING, false,
        "State of stream is not running. Illegal state:%{public}u", state);
    bool result = audioStream_->PauseAudioStream(cmdType);
    if (result) {
        state_ = RENDERER_PAUSED;
    }

    // When user is intentionally pausing, deactivate to remove from audioFocusInfoList_
    int32_t ret = AudioPolicyManager::GetInstance().DeactivateAudioInterrupt(audioInterrupt_);
    if (ret != 0) {
        AUDIO_ERR_LOG("DeactivateAudioInterrupt Failed");
    }
    (void)audioStream_->SetDuckVolume(1.0f);

    return result;
}

bool AudioRendererPrivate::Stop()
{
    AUDIO_INFO_LOG("StreamClientState for Renderer::Stop. id: %{public}u", sessionID_);
    std::lock_guard<std::shared_mutex> lock(rendererMutex_);
    CHECK_AND_RETURN_RET_LOG(!isSwitching_, false,
        "AudioRenderer::Stop failed. Switching state: %{public}d", isSwitching_);
    if (IsNoStreamRenderer()) {
        // When the cellular call stream is stopping, only need to deactivate audio interrupt.
        if (AudioPolicyManager::GetInstance().DeactivateAudioInterrupt(audioInterrupt_) != 0) {
            AUDIO_WARNING_LOG("DeactivateAudioInterrupt Failed");
        }
        state_ = RENDERER_STOPPED;
        return true;
    }

    WriteUnderrunEvent();
    bool result = audioStream_->StopAudioStream();
    if (result) {
        state_ = RENDERER_STOPPED;
    }
    int32_t ret = AudioPolicyManager::GetInstance().DeactivateAudioInterrupt(audioInterrupt_);
    if (ret != 0) {
        AUDIO_WARNING_LOG("DeactivateAudioInterrupt Failed");
    }
    (void)audioStream_->SetDuckVolume(1.0f);

    return result;
}

bool AudioRendererPrivate::Release()
{
    std::unique_lock<std::shared_mutex> lock(rendererMutex_);
    AUDIO_INFO_LOG("StreamClientState for Renderer::Release. id: %{public}u", sessionID_);

    bool result = audioStream_->ReleaseAudioStream();

    // If Stop call was skipped, Release to take care of Deactivation
    (void)AudioPolicyManager::GetInstance().DeactivateAudioInterrupt(audioInterrupt_);

    // Unregister the callaback in policy server
    (void)AudioPolicyManager::GetInstance().UnsetAudioInterruptCallback(sessionID_);

    for (auto id : usedSessionId_) {
        AudioPolicyManager::GetInstance().UnregisterDeviceChangeWithInfoCallback(id);
    }
    lock.unlock();
    RemoveRendererPolicyServiceDiedCallback();

    return result;
}

int32_t AudioRendererPrivate::GetBufferSize(size_t &bufferSize) const
{
    Trace trace("AudioRenderer::GetBufferSize");
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetBufferSize(bufferSize);
}

int32_t AudioRendererPrivate::GetAudioStreamId(uint32_t &sessionID) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetAudioSessionID(sessionID);
}

int32_t AudioRendererPrivate::SetAudioRendererDesc(AudioRendererDesc audioRendererDesc)
{
    ContentType contentType = audioRendererDesc.contentType;
    StreamUsage streamUsage = audioRendererDesc.streamUsage;
    AudioStreamType audioStreamType = IAudioStream::GetStreamType(contentType, streamUsage);
    audioInterrupt_.audioFocusType.streamType = audioStreamType;
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetAudioStreamType(audioStreamType);
}

int32_t AudioRendererPrivate::SetStreamType(AudioStreamType audioStreamType)
{
    audioInterrupt_.audioFocusType.streamType = audioStreamType;
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetAudioStreamType(audioStreamType);
}

int32_t AudioRendererPrivate::SetVolume(float volume) const
{
    UpdateAudioInterruptStrategy(volume);
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetVolume(volume);
}

void AudioRendererPrivate::UpdateAudioInterruptStrategy(float volume) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_LOG(currentStream != nullptr, "audioStream_ is nullptr");
    State currentState = currentStream->GetState();
    if (currentState == NEW || currentState == PREPARED) {
        AUDIO_INFO_LOG("UpdateAudioInterruptStrategy for set volume before RUNNING,  volume=%{public}f", volume);
        isStillMuted_ = (volume == 0);
    } else if (isStillMuted_ && volume > 0) {
        isStillMuted_ = false;
        audioInterrupt_.sessionStrategy.concurrencyMode =
            (originalStrategy_.concurrencyMode == AudioConcurrencyMode::INVALID ?
            AudioConcurrencyMode::DEFAULT : originalStrategy_.concurrencyMode);
        if (currentState == RUNNING) {
            AudioInterrupt audioInterrupt = audioInterrupt_;
            AUDIO_INFO_LOG("UpdateAudioInterruptStrategy for set volume,  volume=%{public}f", volume);
            int ret = AudioPolicyManager::GetInstance().ActivateAudioInterrupt(audioInterrupt, 0, true);
            CHECK_AND_RETURN_LOG(ret == 0, "ActivateAudioInterrupt Failed at SetVolume");
        }
    }
}

float AudioRendererPrivate::GetVolume() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetVolume();
}

int32_t AudioRendererPrivate::SetRenderRate(AudioRendererRate renderRate) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetRenderRate(renderRate);
}

AudioRendererRate AudioRendererPrivate::GetRenderRate() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, RENDER_RATE_NORMAL, "audioStream_ is nullptr");
    return currentStream->GetRenderRate();
}

int32_t AudioRendererPrivate::SetRendererSamplingRate(uint32_t sampleRate) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetRendererSamplingRate(sampleRate);
}

uint32_t AudioRendererPrivate::GetRendererSamplingRate() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetRendererSamplingRate();
}

int32_t AudioRendererPrivate::SetBufferDuration(uint64_t bufferDuration) const
{
    CHECK_AND_RETURN_RET_LOG(bufferDuration >= MINIMUM_BUFFER_SIZE_MSEC && bufferDuration <= MAXIMUM_BUFFER_SIZE_MSEC,
        ERR_INVALID_PARAM, "Error: Please set the buffer duration between 5ms ~ 20ms");
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetBufferSizeInMsec(bufferDuration);
}

int32_t AudioRendererPrivate::SetChannelBlendMode(ChannelBlendMode blendMode)
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetChannelBlendMode(blendMode);
}

AudioRendererInterruptCallbackImpl::AudioRendererInterruptCallbackImpl(const std::shared_ptr<IAudioStream> &audioStream,
    const AudioInterrupt &audioInterrupt)
    : audioStream_(audioStream), audioInterrupt_(audioInterrupt)
{
    AUDIO_DEBUG_LOG("AudioRendererInterruptCallbackImpl constructor");
}

AudioRendererInterruptCallbackImpl::~AudioRendererInterruptCallbackImpl()
{
    AUDIO_DEBUG_LOG("AudioRendererInterruptCallbackImpl: instance destroy");
}

void AudioRendererInterruptCallbackImpl::SaveCallback(const std::weak_ptr<AudioRendererCallback> &callback)
{
    callback_ = callback;
}

void AudioRendererInterruptCallbackImpl::UpdateAudioStream(const std::shared_ptr<IAudioStream> &audioStream)
{
    std::lock_guard<std::mutex> lock(mutex_);
    audioStream_ = audioStream;
}

void AudioRendererInterruptCallbackImpl::NotifyEvent(const InterruptEvent &interruptEvent)
{
    if (cb_ != nullptr && interruptEvent.callbackToApp) {
        cb_->OnInterrupt(interruptEvent);
        AUDIO_DEBUG_LOG("Send interruptEvent to app successfully");
    } else if (cb_ == nullptr) {
        AUDIO_WARNING_LOG("cb_==nullptr, failed to send interruptEvent");
    } else {
        AUDIO_INFO_LOG("callbackToApp is %{public}d", interruptEvent.callbackToApp);
    }
}

bool AudioRendererInterruptCallbackImpl::HandleForceDucking(const InterruptEventInternal &interruptEvent)
{
    float duckVolumeFactor = interruptEvent.duckVolume;
    int32_t ret = audioStream_->SetDuckVolume(duckVolumeFactor);
    CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, false, "Failed to set duckVolumeFactor(instance) %{public}f",
        duckVolumeFactor);

    AUDIO_INFO_LOG("Set duckVolumeFactor %{public}f successfully.", duckVolumeFactor);
    return true;
}

void AudioRendererInterruptCallbackImpl::NotifyForcePausedToResume(const InterruptEventInternal &interruptEvent)
{
    // Change InterruptForceType to Share, Since app will take care of resuming
    InterruptEvent interruptEventResume {interruptEvent.eventType, INTERRUPT_SHARE,
                                         interruptEvent.hintType};
    NotifyEvent(interruptEventResume);
}

void AudioRendererInterruptCallbackImpl::HandleAndNotifyForcedEvent(const InterruptEventInternal &interruptEvent)
{
    State currentState = audioStream_->GetState();
    audioStream_->GetAudioSessionID(sessionID_);
    switch (interruptEvent.hintType) {
        case INTERRUPT_HINT_PAUSE:
            if (currentState == RUNNING || currentState == PREPARED) {
                (void)audioStream_->PauseAudioStream(); // Just Pause, do not deactivate here
                (void)audioStream_->SetDuckVolume(1.0f);
                isForcePaused_ = true;
            } else {
                AUDIO_WARNING_LOG("sessionId: %{public}u, state: %{public}d. No need to pause",
                    sessionID_, static_cast<int32_t>(currentState));
                return;
            }
            break;
        case INTERRUPT_HINT_RESUME:
            if ((currentState != PAUSED && currentState != PREPARED) || !isForcePaused_) {
                AUDIO_WARNING_LOG("sessionId: %{public}u, State: %{public}d or not force pause before",
                    sessionID_, static_cast<int32_t>(currentState));
                return;
            }
            isForcePaused_ = false;
            NotifyForcePausedToResume(interruptEvent);
            return; // return, sending callback is taken care in NotifyForcePausedToResume
        case INTERRUPT_HINT_STOP:
            (void)audioStream_->StopAudioStream();
            (void)audioStream_->SetDuckVolume(1.0f);
            break;
        case INTERRUPT_HINT_DUCK:
            if (!HandleForceDucking(interruptEvent)) {
                AUDIO_WARNING_LOG("Failed to duck forcely, don't notify app");
                return;
            }
            isForceDucked_ = true;
            break;
        case INTERRUPT_HINT_UNDUCK:
            CHECK_AND_RETURN_LOG(isForceDucked_, "It is not forced ducked, don't unduck or notify app");
            (void)audioStream_->SetDuckVolume(1.0f);
            AUDIO_INFO_LOG("Unduck Volume successfully");
            isForceDucked_ = false;
            break;
        default: // If the hintType is NONE, don't need to send callbacks
            return;
    }
    // Notify valid forced event callbacks to app
    NotifyForcedEvent(interruptEvent);
}

void AudioRendererInterruptCallbackImpl::NotifyForcedEvent(const InterruptEventInternal &interruptEvent)
{
    InterruptEvent interruptEventForced {interruptEvent.eventType, interruptEvent.forceType, interruptEvent.hintType,
        interruptEvent.callbackToApp};
    if (interruptEventForced.hintType == INTERRUPT_HINT_RESUME) {
        // Reusme event should be INTERRUPT_SHARE type. Change the force type before sending the interrupt event.
        interruptEventForced.forceType = INTERRUPT_SHARE;
    }
    NotifyEvent(interruptEventForced);
}

void AudioRendererInterruptCallbackImpl::OnInterrupt(const InterruptEventInternal &interruptEvent)
{
    std::lock_guard<std::mutex> lock(mutex_);

    cb_ = callback_.lock();
    InterruptForceType forceType = interruptEvent.forceType;

    if (audioStream_ != nullptr) {
        audioStream_->GetAudioSessionID(sessionID_);
    }
    AUDIO_INFO_LOG("sessionId: %{public}u, forceType: %{public}d, hintType: %{public}d",
        sessionID_, forceType, interruptEvent.hintType);

    if (forceType != INTERRUPT_FORCE) { // INTERRUPT_SHARE
        AUDIO_DEBUG_LOG("INTERRUPT_SHARE. Let app handle the event");
        InterruptEvent interruptEventShared {interruptEvent.eventType, interruptEvent.forceType,
            interruptEvent.hintType, interruptEvent.callbackToApp};
        NotifyEvent(interruptEventShared);
        return;
    }

    CHECK_AND_RETURN_LOG(audioStream_ != nullptr,
        "Stream is not alive. No need to take forced action");

    HandleAndNotifyForcedEvent(interruptEvent);
}

AudioRendererConcurrencyCallbackImpl::AudioRendererConcurrencyCallbackImpl()
{
    AUDIO_INFO_LOG("AudioRendererConcurrencyCallbackImpl ctor");
}

AudioRendererConcurrencyCallbackImpl::~AudioRendererConcurrencyCallbackImpl()
{
    AUDIO_INFO_LOG("AudioRendererConcurrencyCallbackImpl dtor");
}

void AudioRendererConcurrencyCallbackImpl::OnConcedeStream()
{
    std::lock_guard<std::mutex> lock(mutex_);
    CHECK_AND_RETURN_LOG(renderer_ != nullptr, "renderer is nullptr");
    renderer_->ConcedeStream();
}

int32_t AudioRendererPrivate::InitAudioConcurrencyCallback()
{
    if (audioConcurrencyCallback_ == nullptr) {
        audioConcurrencyCallback_ = std::make_shared<AudioRendererConcurrencyCallbackImpl>();
        CHECK_AND_RETURN_RET_LOG(audioConcurrencyCallback_ != nullptr, ERROR, "Memory Allocation Failed !!");
    }
    audioConcurrencyCallback_->SetAudioRendererObj(this);
    return AudioPolicyManager::GetInstance().SetAudioConcurrencyCallback(sessionID_, audioConcurrencyCallback_);
}

void AudioStreamCallbackRenderer::SaveCallback(const std::weak_ptr<AudioRendererCallback> &callback)
{
    callback_ = callback;
}

void AudioStreamCallbackRenderer::OnStateChange(const State state, const StateChangeCmdType cmdType)
{
    std::shared_ptr<AudioRendererCallback> cb = callback_.lock();
    CHECK_AND_RETURN_LOG(cb != nullptr, "cb == nullptr.");

    cb->OnStateChange(static_cast<RendererState>(state), cmdType);
}

std::vector<AudioSampleFormat> AudioRenderer::GetSupportedFormats()
{
    return AUDIO_SUPPORTED_FORMATS;
}

std::vector<AudioSamplingRate> AudioRenderer::GetSupportedSamplingRates()
{
    return AUDIO_SUPPORTED_SAMPLING_RATES;
}

std::vector<AudioChannel> AudioRenderer::GetSupportedChannels()
{
    return RENDERER_SUPPORTED_CHANNELS;
}

std::vector<AudioEncodingType> AudioRenderer::GetSupportedEncodingTypes()
{
    return AUDIO_SUPPORTED_ENCODING_TYPES;
}

int32_t AudioRendererPrivate::SetRenderMode(AudioRenderMode renderMode)
{
    AUDIO_INFO_LOG("Render mode: %{public}d", renderMode);
    audioRenderMode_ = renderMode;
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    if (renderMode == RENDER_MODE_CALLBACK && rendererInfo_.originalFlag != AUDIO_FLAG_FORCED_NORMAL &&
        (rendererInfo_.streamUsage == STREAM_USAGE_VOICE_COMMUNICATION ||
        rendererInfo_.streamUsage == STREAM_USAGE_VIDEO_COMMUNICATION)) {
        // both fast and direct VoIP renderer can only use RENDER_MODE_CALLBACK;
        int32_t flags = AudioPolicyManager::GetInstance().GetPreferredOutputStreamType(rendererInfo_);
        uint32_t sessionId = 0;
        int32_t ret = currentStream->GetAudioSessionID(sessionId);
        CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ret, "Get audio session Id failed");
        uint32_t newSessionId = 0;
        IAudioStream::StreamClass streamClass = IAudioStream::PA_STREAM;
        if (flags == AUDIO_FLAG_VOIP_FAST) {
            AUDIO_INFO_LOG("Switch to fast voip stream");
            streamClass = IAudioStream::VOIP_STREAM;
        } else if (flags == AUDIO_FLAG_VOIP_DIRECT && isDirectVoipSupported_) {
            AUDIO_INFO_LOG("Switch to direct voip stream");
            rendererInfo_.rendererFlags = AUDIO_FLAG_VOIP_DIRECT;
            streamClass = IAudioStream::PA_STREAM;
        }
        if (!SwitchToTargetStream(streamClass, newSessionId, AudioStreamDeviceChangeReasonExt::ExtEnum::UNKNOWN)) {
            AUDIO_ERR_LOG("Switch to target stream failed");
            return ERROR;
        }
        // audioStream_ has been updated by SwitchToTargetStream. Update currentStream before SetRenderMode.
        currentStream = GetInnerStream();
        usedSessionId_.push_back(newSessionId);
        ret = AudioPolicyManager::GetInstance().RegisterDeviceChangeWithInfoCallback(newSessionId,
            outputDeviceChangeCallback_);
        CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ret, "Register device change callback for new session failed");
    }

    return currentStream->SetRenderMode(renderMode);
}

AudioRenderMode AudioRendererPrivate::GetRenderMode() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, RENDER_MODE_NORMAL, "audioStream_ is nullptr");
    return currentStream->GetRenderMode();
}

int32_t AudioRendererPrivate::GetBufferDesc(BufferDesc &bufDesc) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    int32_t ret = currentStream->GetBufferDesc(bufDesc);
    return ret;
}

int32_t AudioRendererPrivate::Enqueue(const BufferDesc &bufDesc) const
{
    Trace trace("AudioRenderer::Enqueue");
    MockPcmData(bufDesc.buffer, bufDesc.bufLength);
    DumpFileUtil::WriteDumpFile(dumpFile_, static_cast<void *>(bufDesc.buffer), bufDesc.bufLength);
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    int32_t ret = currentStream->Enqueue(bufDesc);
    return ret;
}

int32_t AudioRendererPrivate::Clear() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->Clear();
}

int32_t AudioRendererPrivate::GetBufQueueState(BufferQueueState &bufState) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetBufQueueState(bufState);
}

int32_t AudioRendererPrivate::SetRendererWriteCallback(const std::shared_ptr<AudioRendererWriteCallback> &callback)
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetRendererWriteCallback(callback);
}

int32_t AudioRendererPrivate::SetRendererFirstFrameWritingCallback(
    const std::shared_ptr<AudioRendererFirstFrameWritingCallback> &callback)
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetRendererFirstFrameWritingCallback(callback);
}

void AudioRendererPrivate::SetInterruptMode(InterruptMode mode)
{
    AUDIO_INFO_LOG("InterruptMode %{public}d", mode);
    if (audioInterrupt_.mode == mode) {
        return;
    } else if (mode != SHARE_MODE && mode != INDEPENDENT_MODE) {
        AUDIO_ERR_LOG("Invalid interrupt mode!");
        return;
    }
    audioInterrupt_.mode = mode;
}

void AudioRendererPrivate::SetSilentModeAndMixWithOthers(bool on)
{
    Trace trace(std::string("AudioRenderer::SetSilentModeAndMixWithOthers:") + (on ? "on" : "off"));
    std::shared_lock<std::shared_mutex> sharedLockSwitch(rendererMutex_);
    std::lock_guard<std::mutex> lock(silentModeAndMixWithOthersMutex_);
    if (static_cast<RendererState>(audioStream_->GetState()) == RENDERER_RUNNING) {
        if (audioStream_->GetSilentModeAndMixWithOthers() && !on) {
            audioInterrupt_.sessionStrategy.concurrencyMode = AudioConcurrencyMode::DEFAULT;
            int32_t ret = AudioPolicyManager::GetInstance().ActivateAudioInterrupt(audioInterrupt_, 0, true);
            CHECK_AND_RETURN_LOG(ret == SUCCESS, "ActivateAudioInterrupt Failed");
            audioStream_->SetSilentModeAndMixWithOthers(on);
            return;
        } else if (!audioStream_->GetSilentModeAndMixWithOthers() && on) {
            audioStream_->SetSilentModeAndMixWithOthers(on);
            audioInterrupt_.sessionStrategy.concurrencyMode = AudioConcurrencyMode::SILENT;
            int32_t ret = AudioPolicyManager::GetInstance().ActivateAudioInterrupt(audioInterrupt_, 0, true);
            CHECK_AND_RETURN_LOG(ret == SUCCESS, "ActivateAudioInterrupt Failed");
            return;
        }
    }
    audioStream_->SetSilentModeAndMixWithOthers(on);
}

bool AudioRendererPrivate::GetSilentModeAndMixWithOthers()
{
    std::lock_guard<std::mutex> lock(silentModeAndMixWithOthersMutex_);
    return audioStream_->GetSilentModeAndMixWithOthers();
}

int32_t AudioRendererPrivate::SetParallelPlayFlag(bool parallelPlayFlag)
{
    AUDIO_PRERELEASE_LOGI("parallelPlayFlag %{public}d", parallelPlayFlag);
    audioInterrupt_.parallelPlayFlag = parallelPlayFlag;
    return SUCCESS;
}

int32_t AudioRendererPrivate::SetLowPowerVolume(float volume) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetLowPowerVolume(volume);
}

float AudioRendererPrivate::GetLowPowerVolume() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetLowPowerVolume();
}

int32_t AudioRendererPrivate::SetOffloadAllowed(bool isAllowed)
{
    AUDIO_PRERELEASE_LOGI("offload allowed: %{public}d", isAllowed);
    rendererInfo_.isOffloadAllowed = isAllowed;
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    currentStream->SetRendererInfo(rendererInfo_);
    return SUCCESS;
}

int32_t AudioRendererPrivate::SetOffloadMode(int32_t state, bool isAppBack) const
{
    AUDIO_INFO_LOG("set offload mode for session %{public}u", sessionID_);
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetOffloadMode(state, isAppBack);
}

int32_t AudioRendererPrivate::UnsetOffloadMode() const
{
    AUDIO_INFO_LOG("session %{public}u session unset offload", sessionID_);
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    int32_t ret = currentStream->UnsetOffloadMode();
    CHECK_AND_RETURN_RET_LOG(ret == SUCCESS, ret, "unset offload failed");
    return SUCCESS;
}

float AudioRendererPrivate::GetSingleStreamVolume() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetSingleStreamVolume();
}

float AudioRendererPrivate::GetMinStreamVolume() const
{
    return AudioPolicyManager::GetInstance().GetMinStreamVolume();
}

float AudioRendererPrivate::GetMaxStreamVolume() const
{
    return AudioPolicyManager::GetInstance().GetMaxStreamVolume();
}

int32_t AudioRendererPrivate::GetCurrentOutputDevices(AudioDeviceDescriptor &deviceInfo) const
{
    std::vector<std::shared_ptr<AudioRendererChangeInfo>> audioRendererChangeInfos;
    uint32_t sessionId = static_cast<uint32_t>(-1);
    int32_t ret = GetAudioStreamId(sessionId);
    CHECK_AND_RETURN_RET_LOG(!ret, ret, " Get sessionId failed");

    ret = AudioPolicyManager::GetInstance().GetCurrentRendererChangeInfos(audioRendererChangeInfos);
    CHECK_AND_RETURN_RET_LOG(!ret, ret, "Get Current Renderer devices failed");

    for (auto it = audioRendererChangeInfos.begin(); it != audioRendererChangeInfos.end(); it++) {
        if ((*it)->sessionId == static_cast<int32_t>(sessionId)) {
            deviceInfo = (*it)->outputDeviceInfo;
        }
    }
    return SUCCESS;
}

uint32_t AudioRendererPrivate::GetUnderflowCount() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetUnderflowCount();
}


void AudioRendererPrivate::SetAudioRendererErrorCallback(std::shared_ptr<AudioRendererErrorCallback> errorCallback)
{
    std::shared_lock sharedLock(rendererMutex_);
    std::lock_guard lock(audioRendererErrCallbackMutex_);
    audioRendererErrorCallback_ = errorCallback;
}

int32_t AudioRendererPrivate::RegisterAudioPolicyServerDiedCb(const int32_t clientPid,
    const std::shared_ptr<AudioRendererPolicyServiceDiedCallback> &callback)
{
    AUDIO_INFO_LOG("RegisterAudioPolicyServerDiedCb client id: %{public}d", clientPid);
    CHECK_AND_RETURN_RET_LOG(callback != nullptr, ERR_INVALID_PARAM, "callback is null");

    std::lock_guard<std::mutex> lock(policyServiceDiedCallbackMutex_);

    policyServiceDiedCallback_ = callback;
    return AudioPolicyManager::GetInstance().RegisterAudioPolicyServerDiedCb(clientPid, callback);
}

int32_t AudioRendererPrivate::UnregisterAudioPolicyServerDiedCb(const int32_t clientPid)
{
    AUDIO_INFO_LOG("UnregisterAudioPolicyServerDiedCb client id: %{public}d", clientPid);
    return AudioPolicyManager::GetInstance().UnregisterAudioPolicyServerDiedCb(clientPid);
}

int32_t AudioRendererPrivate::RegisterOutputDeviceChangeWithInfoCallback(
    const std::shared_ptr<AudioRendererOutputDeviceChangeCallback> &callback)
{
    AUDIO_INFO_LOG("in");
    if (callback == nullptr) {
        AUDIO_ERR_LOG("callback is null");
        return ERR_INVALID_PARAM;
    }

    outputDeviceChangeCallback_->SaveCallback(callback);
    AUDIO_DEBUG_LOG("successful!");
    return SUCCESS;
}

int32_t AudioRendererPrivate::UnregisterOutputDeviceChangeWithInfoCallback()
{
    AUDIO_INFO_LOG("Unregister all");

    outputDeviceChangeCallback_->RemoveCallback();
    return SUCCESS;
}

int32_t AudioRendererPrivate::UnregisterOutputDeviceChangeWithInfoCallback(
    const std::shared_ptr<AudioRendererOutputDeviceChangeCallback> &callback)
{
    AUDIO_INFO_LOG("in");

    outputDeviceChangeCallback_->RemoveCallback(callback);
    return SUCCESS;
}

int32_t AudioRendererPrivate::SetSwitchInfo(IAudioStream::SwitchInfo info, std::shared_ptr<IAudioStream> audioStream)
{
    CHECK_AND_RETURN_RET_LOG(audioStream, ERROR, "stream is nullptr");

    audioStream->SetStreamTrackerState(false);
    audioStream->SetClientID(info.clientPid, info.clientUid, appInfo_.appTokenId, appInfo_.appFullTokenId);
    audioStream->SetPrivacyType(info.privacyType);
    audioStream->SetRendererInfo(info.rendererInfo);
    audioStream->SetCapturerInfo(info.capturerInfo);
    int32_t res = audioStream->SetAudioStreamInfo(info.params, rendererProxyObj_);
    CHECK_AND_RETURN_RET_LOG(res == SUCCESS, ERROR, "SetAudioStreamInfo failed");
    audioStream->SetDefaultOutputDevice(info.defaultOutputDevice);
    audioStream->SetRenderMode(info.renderMode);
    audioStream->SetAudioEffectMode(info.effectMode);
    audioStream->SetVolume(info.volume);
    audioStream->SetUnderflowCount(info.underFlowCount);

    if (info.userSettedPreferredFrameSize.has_value()) {
        audioStream->SetPreferredFrameSize(info.userSettedPreferredFrameSize.value());
    }

    audioStream->SetSilentModeAndMixWithOthers(info.silentModeAndMixWithOthers);

    if (speed_.has_value()) {
        audioStream->SetSpeed(speed_.value());
    }

    // set callback
    if ((info.renderPositionCb != nullptr) && (info.frameMarkPosition > 0)) {
        audioStream->SetRendererPositionCallback(info.frameMarkPosition, info.renderPositionCb);
    }

    if ((info.capturePositionCb != nullptr) && (info.frameMarkPosition > 0)) {
        audioStream->SetCapturerPositionCallback(info.frameMarkPosition, info.capturePositionCb);
    }

    if ((info.renderPeriodPositionCb != nullptr) && (info.framePeriodNumber > 0)) {
        audioStream->SetRendererPeriodPositionCallback(info.framePeriodNumber, info.renderPeriodPositionCb);
    }

    if ((info.capturePeriodPositionCb != nullptr) && (info.framePeriodNumber > 0)) {
        audioStream->SetCapturerPeriodPositionCallback(info.framePeriodNumber, info.capturePeriodPositionCb);
    }

    audioStream->SetStreamCallback(info.audioStreamCallback);
    audioStream->SetRendererWriteCallback(info.rendererWriteCallback);

    audioStream->SetRendererFirstFrameWritingCallback(info.rendererFirstFrameWritingCallback);
    return SUCCESS;
}

// The only function that updates AudioRendererPrivate::audioStream_ in its life cycle.
void AudioRendererPrivate::UpdateRendererAudioStream(const std::shared_ptr<IAudioStream> &newAudioStream)
{
    std::unique_lock<std::shared_mutex> lock(streamMutex_);
    audioStream_ = newAudioStream;
    lock.unlock();
    if (audioInterruptCallback_ != nullptr) {
        std::shared_ptr<AudioRendererInterruptCallbackImpl> interruptCbImpl =
            std::static_pointer_cast<AudioRendererInterruptCallbackImpl>(audioInterruptCallback_);
        interruptCbImpl->UpdateAudioStream(audioStream_);
    }
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
RendererState AudioRendererPrivate::GetStatusInner()
{
    if (IsNoStreamRenderer()) {
        return state_;
    }
    return static_cast<RendererState>(audioStream_->GetState());
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
void AudioRendererPrivate::SetAudioPrivacyTypeInner(AudioPrivacyType privacyType)
{
    CHECK_AND_RETURN_LOG(audioStream_ != nullptr, "audioStream_ is nullptr");
    privacyType_ = privacyType;
    audioStream_->SetPrivacyType(privacyType);
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
int32_t AudioRendererPrivate::GetAudioStreamIdInner(uint32_t &sessionID) const
{
    return audioStream_->GetAudioSessionID(sessionID);
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
float AudioRendererPrivate::GetVolumeInner() const
{
    return audioStream_->GetVolume();
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
uint32_t AudioRendererPrivate::GetUnderflowCountInner() const
{
    return audioStream_->GetUnderflowCount();
}

// Only called in SwitchToTargetStream, with AudioRendererPrivate::rendererMutex_ held.
void AudioRendererPrivate::InitSwitchInfo(IAudioStream::StreamClass targetClass, IAudioStream::SwitchInfo &info)
{
    audioStream_->GetSwitchInfo(info);
    if (targetClass == IAudioStream::VOIP_STREAM) {
        info.rendererInfo.originalFlag = AUDIO_FLAG_VOIP_FAST;
    }

    if (rendererInfo_.rendererFlags == AUDIO_FLAG_VOIP_DIRECT) {
        info.rendererInfo.originalFlag = AUDIO_FLAG_VOIP_DIRECT;
        info.rendererInfo.rendererFlags = AUDIO_FLAG_VOIP_DIRECT;
        info.rendererFlags = AUDIO_FLAG_VOIP_DIRECT;
    } else if (rendererInfo_.rendererFlags == AUDIO_FLAG_DIRECT) {
        info.rendererInfo.pipeType = PIPE_TYPE_DIRECT_MUSIC;
        info.rendererFlags = AUDIO_FLAG_DIRECT;
    }
    info.params.originalSessionId = sessionID_;
    return;
}

// Only called in SwitchToTargetStream, with AudioRendererPrivate::rendererMutex_ held.
void AudioRendererPrivate::UpdateFramesWritten()
{
    int64_t framesWritten = audioStream_->GetFramesWritten();
    if (framesWritten > 0) {
        framesAlreadyWritten_ += framesWritten;
        AUDIO_INFO_LOG("Frames already written: %{public}" PRId64 ", current stream value: %{public}" PRId64 ".",
            framesAlreadyWritten_, framesWritten);
    }
}

// Set new stream info before switching to new stream.
bool AudioRendererPrivate::InitTargetStream(IAudioStream::SwitchInfo &info,
    std::shared_ptr<IAudioStream> &newAudioStream)
{
    int32_t initResult = SetSwitchInfo(info, newAudioStream);
    if (initResult != SUCCESS && info.rendererInfo.originalFlag != AUDIO_FLAG_NORMAL) {
        AUDIO_ERR_LOG("Re-create stream failed, crate normal ipc stream");
        isFastRenderer_ = false;
        newAudioStream = IAudioStream::GetPlaybackStream(IAudioStream::PA_STREAM, info.params,
            info.eStreamType, appInfo_.appPid);
        CHECK_AND_RETURN_RET_LOG(newAudioStream != nullptr, false, "Get ipc stream failed");
        initResult = SetSwitchInfo(info, newAudioStream);
        CHECK_AND_RETURN_RET_LOG(initResult == SUCCESS, false, "Init ipc strean failed");
    }
    return (initResult == SUCCESS);
}

bool AudioRendererPrivate::SwitchToTargetStream(IAudioStream::StreamClass targetClass, uint32_t &newSessionId,
    const AudioStreamDeviceChangeReasonExt reason)
{
    bool switchResult = false;
    if (audioStream_) {
        Trace trace("SwitchToTargetStream");
        std::shared_ptr<IAudioStream> oldAudioStream = nullptr;
        std::lock_guard<std::shared_mutex> lock(rendererMutex_);
        isSwitching_ = true;
        RendererState previousState = GetStatusInner();
        AUDIO_INFO_LOG("Previous stream state: %{public}d, original sessionId: %{public}u", previousState, sessionID_);
        if (previousState == RENDERER_RUNNING) {
            CHECK_AND_RETURN_RET_LOG(audioStream_->StopAudioStream(), false, "StopAudioStream failed.");
        }
        IAudioStream::SwitchInfo info;
        InitSwitchInfo(targetClass, info);
        UpdateFramesWritten();
        switchResult = audioStream_->ReleaseAudioStream(true, true);
        CHECK_AND_RETURN_RET_LOG(switchResult, false, "release old stream failed.");

        // create new IAudioStream
        std::shared_ptr<IAudioStream> newAudioStream = IAudioStream::GetPlaybackStream(targetClass, info.params,
            info.eStreamType, appInfo_.appPid);
        CHECK_AND_RETURN_RET_LOG(newAudioStream != nullptr, false, "SetParams GetPlayBackStream failed.");
        AUDIO_INFO_LOG("Get new stream success!");

        // set new stream info
        switchResult = InitTargetStream(info, newAudioStream);
        CHECK_AND_RETURN_RET_LOG(switchResult, false, "Init target stream failed");

        if (previousState == RENDERER_RUNNING) {
            // restart audio stream
            switchResult = newAudioStream->StartAudioStream(CMD_FROM_CLIENT, reason);
            CHECK_AND_RETURN_RET_LOG(switchResult, false, "start new stream failed.");
        }

        // Update audioStream_ to newAudioStream in both AudioRendererPrivate and AudioInterruptCallbackImpl.
        oldAudioStream = audioStream_;
        UpdateRendererAudioStream(newAudioStream);
        isFastRenderer_ = IAudioStream::IsFastStreamClass(targetClass);
        isSwitching_ = false;
        audioStream_->GetAudioSessionID(newSessionId);
        switchResult = true;
    }
    WriteSwitchStreamLogMsg();
    return switchResult;
}

void AudioRendererPrivate::WriteSwitchStreamLogMsg()
{
    std::shared_ptr<Media::MediaMonitor::EventBean> bean = std::make_shared<Media::MediaMonitor::EventBean>(
        Media::MediaMonitor::ModuleId::AUDIO, Media::MediaMonitor::EventId::AUDIO_PIPE_CHANGE,
        Media::MediaMonitor::EventType::BEHAVIOR_EVENT);
    bean->Add("CLIENT_UID", appInfo_.appUid);
    bean->Add("IS_PLAYBACK", 1);
    bean->Add("STREAM_TYPE", rendererInfo_.streamUsage);
    bean->Add("PIPE_TYPE_BEFORE_CHANGE", PIPE_TYPE_LOWLATENCY_OUT);
    bean->Add("PIPE_TYPE_AFTER_CHANGE", PIPE_TYPE_NORMAL_OUT);
    bean->Add("REASON", Media::MediaMonitor::DEVICE_CHANGE_FROM_FAST);
    Media::MediaMonitor::MediaMonitorManager::GetInstance().WriteLogMsg(bean);
}

void AudioRendererPrivate::SwitchStream(const uint32_t sessionId, const int32_t streamFlag,
    const AudioStreamDeviceChangeReasonExt reason)
{
    IAudioStream::StreamClass targetClass = IAudioStream::PA_STREAM;
    switch (streamFlag) {
        case AUDIO_FLAG_NORMAL:
            rendererInfo_.rendererFlags = AUDIO_FLAG_NORMAL;
            targetClass = IAudioStream::PA_STREAM;
            break;
        case AUDIO_FLAG_MMAP:
            rendererInfo_.rendererFlags = AUDIO_FLAG_MMAP;
            targetClass = IAudioStream::FAST_STREAM;
            break;
        case AUDIO_FLAG_VOIP_FAST:
            rendererInfo_.rendererFlags = AUDIO_FLAG_VOIP_FAST;
            targetClass = IAudioStream::VOIP_STREAM;
            break;
        case AUDIO_FLAG_VOIP_DIRECT:
            rendererInfo_.rendererFlags = (isDirectVoipSupported_ && audioRenderMode_ == RENDER_MODE_CALLBACK) ?
                AUDIO_FLAG_VOIP_DIRECT : AUDIO_FLAG_NORMAL;
            targetClass = IAudioStream::PA_STREAM;
            break;
        case AUDIO_FLAG_DIRECT:
            rendererInfo_.rendererFlags = AUDIO_FLAG_DIRECT;
            break;
        default:
            AUDIO_INFO_LOG("unknown stream flag");
            break;
    }
    if (rendererInfo_.originalFlag == AUDIO_FLAG_FORCED_NORMAL) {
        rendererInfo_.rendererFlags = AUDIO_FLAG_NORMAL;
        targetClass = IAudioStream::PA_STREAM;
    }

    uint32_t newSessionId = 0;
    if (!SwitchToTargetStream(targetClass, newSessionId, reason)) {
        int32_t ret = AudioPolicyManager::GetInstance().DeactivateAudioInterrupt(audioInterrupt_);
        CHECK_AND_RETURN_LOG(ret == 0, "DeactivateAudioInterrupt Failed");
        if (audioRendererErrorCallback_) {
            audioRendererErrorCallback_->OnError(ERROR_SYSTEM);
        }
    }
    usedSessionId_.push_back(newSessionId);
    int32_t ret = AudioPolicyManager::GetInstance().RegisterDeviceChangeWithInfoCallback(newSessionId,
        outputDeviceChangeCallback_);
    CHECK_AND_RETURN_LOG(ret == SUCCESS, "Register device change callback for new session failed");
}

void OutputDeviceChangeWithInfoCallbackImpl::OnDeviceChangeWithInfo(
    const uint32_t sessionId, const AudioDeviceDescriptor &deviceInfo, const AudioStreamDeviceChangeReasonExt reason)
{
    AUDIO_INFO_LOG("OnRendererStateChange");
    std::vector<std::shared_ptr<AudioRendererOutputDeviceChangeCallback>> callbacks;

    {
        std::lock_guard<std::mutex> lock(callbackMutex_);
        callbacks = callbacks_;
    }

    for (auto &cb : callbacks) {
        if (cb != nullptr) {
            cb->OnOutputDeviceChange(deviceInfo, reason);
        }
    }

    AUDIO_INFO_LOG("sessionId: %{public}u, deviceType: %{public}d reason: %{public}d size: %{public}zu",
        sessionId, static_cast<int>(deviceInfo.deviceType_), static_cast<int>(reason), callbacks.size());
}

void OutputDeviceChangeWithInfoCallbackImpl::OnRecreateStreamEvent(const uint32_t sessionId, const int32_t streamFlag,
    const AudioStreamDeviceChangeReasonExt reason)
{
    std::lock_guard<std::mutex> lock(audioRendererObjMutex_);
    AUDIO_INFO_LOG("Enter, session id: %{public}d, stream flag: %{public}d", sessionId, streamFlag);
    CHECK_AND_RETURN_LOG(renderer_ != nullptr, "renderer_ is nullptr");
    renderer_->SwitchStream(sessionId, streamFlag, reason);
}

AudioEffectMode AudioRendererPrivate::GetAudioEffectMode() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, EFFECT_NONE, "audioStream_ is nullptr");
    return currentStream->GetAudioEffectMode();
}

int64_t AudioRendererPrivate::GetFramesWritten() const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return framesAlreadyWritten_ + currentStream->GetFramesWritten();
}

int32_t AudioRendererPrivate::SetAudioEffectMode(AudioEffectMode effectMode) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetAudioEffectMode(effectMode);
}

int32_t AudioRendererPrivate::SetVolumeWithRamp(float volume, int32_t duration)
{
    AUDIO_INFO_LOG("volume:%{public}f duration:%{public}d", volume, duration);
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->SetVolumeWithRamp(volume, duration);
}

void AudioRendererPrivate::SetPreferredFrameSize(int32_t frameSize)
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_LOG(currentStream != nullptr, "audioStream_ is nullptr");
    currentStream->SetPreferredFrameSize(frameSize);
}

void AudioRendererPrivate::GetAudioInterrupt(AudioInterrupt &audioInterrupt)
{
    audioInterrupt = audioInterrupt_;
}

// Only called AudioRendererPrivate::Stop(), with AudioRendererPrivate::rendererMutex_ held.
void AudioRendererPrivate::WriteUnderrunEvent() const
{
    AUDIO_INFO_LOG("AudioRendererPrivate WriteUnderrunEvent!");
    if (GetUnderflowCountInner() < WRITE_UNDERRUN_NUM) {
        return;
    }
    AudioPipeType pipeType = PIPE_TYPE_NORMAL_OUT;
    IAudioStream::StreamClass streamClass = audioStream_->GetStreamClass();
    if (streamClass == IAudioStream::FAST_STREAM) {
        pipeType = PIPE_TYPE_LOWLATENCY_OUT;
    } else if (streamClass == IAudioStream::PA_STREAM) {
        if (audioStream_->GetOffloadEnable()) {
            pipeType = PIPE_TYPE_OFFLOAD;
        } else if (audioStream_->GetSpatializationEnabled()) {
            pipeType = PIPE_TYPE_SPATIALIZATION;
        } else if (audioStream_->GetHighResolutionEnabled()) {
            pipeType = PIPE_TYPE_HIGHRESOLUTION;
        }
    }
    std::shared_ptr<Media::MediaMonitor::EventBean> bean = std::make_shared<Media::MediaMonitor::EventBean>(
        Media::MediaMonitor::ModuleId::AUDIO, Media::MediaMonitor::EventId::PERFORMANCE_UNDER_OVERRUN_STATS,
        Media::MediaMonitor::EventType::FREQUENCY_AGGREGATION_EVENT);
    bean->Add("IS_PLAYBACK", 1);
    bean->Add("CLIENT_UID", appInfo_.appUid);
    bean->Add("PIPE_TYPE", pipeType);
    bean->Add("STREAM_TYPE", rendererInfo_.streamUsage);
    Media::MediaMonitor::MediaMonitorManager::GetInstance().WriteLogMsg(bean);
}

int32_t AudioRendererPrivate::RegisterRendererPolicyServiceDiedCallback()
{
    std::lock_guard<std::mutex> lock(rendererPolicyServiceDiedCbMutex_);
    AUDIO_DEBUG_LOG("RegisterRendererPolicyServiceDiedCallback");
    if (!audioPolicyServiceDiedCallback_) {
        audioPolicyServiceDiedCallback_ = std::make_shared<RendererPolicyServiceDiedCallback>();
        if (!audioPolicyServiceDiedCallback_) {
            AUDIO_ERR_LOG("Memory allocation failed!!");
            return ERROR;
        }
        AudioPolicyManager::GetInstance().RegisterAudioStreamPolicyServerDiedCb(audioPolicyServiceDiedCallback_);
        audioPolicyServiceDiedCallback_->SetAudioRendererObj(this);
        audioPolicyServiceDiedCallback_->SetAudioInterrupt(audioInterrupt_);
    }
    return SUCCESS;
}

int32_t AudioRendererPrivate::RemoveRendererPolicyServiceDiedCallback()
{
    std::lock_guard<std::mutex> lock(rendererPolicyServiceDiedCbMutex_);
    AUDIO_DEBUG_LOG("RemoveRendererPolicyServiceDiedCallback");
    if (audioPolicyServiceDiedCallback_) {
        int32_t ret = AudioPolicyManager::GetInstance().UnregisterAudioStreamPolicyServerDiedCb(
            audioPolicyServiceDiedCallback_);
        if (ret != 0) {
            AUDIO_ERR_LOG("RemoveRendererPolicyServiceDiedCallback failed");
            audioPolicyServiceDiedCallback_ = nullptr;
            return ERROR;
        }
    }
    audioPolicyServiceDiedCallback_ = nullptr;
    return SUCCESS;
}

RendererPolicyServiceDiedCallback::RendererPolicyServiceDiedCallback()
{
    AUDIO_DEBUG_LOG("RendererPolicyServiceDiedCallback create");
}

RendererPolicyServiceDiedCallback::~RendererPolicyServiceDiedCallback()
{
    AUDIO_DEBUG_LOG("RendererPolicyServiceDiedCallback destroy");
    if (restoreThread_ != nullptr && restoreThread_->joinable()) {
        restoreThread_->join();
        restoreThread_.reset();
        restoreThread_ = nullptr;
    }
}

void RendererPolicyServiceDiedCallback::SetAudioRendererObj(AudioRendererPrivate *rendererObj)
{
    renderer_ = rendererObj;
}

void RendererPolicyServiceDiedCallback::SetAudioInterrupt(AudioInterrupt &audioInterrupt)
{
    audioInterrupt_ = audioInterrupt;
}

void RendererPolicyServiceDiedCallback::OnAudioPolicyServiceDied()
{
    AUDIO_INFO_LOG("RendererPolicyServiceDiedCallback::OnAudioPolicyServiceDied");
    if (restoreThread_ != nullptr) {
        restoreThread_->detach();
    }
    restoreThread_ = std::make_unique<std::thread>([this] { this->RestoreTheadLoop(); });
    pthread_setname_np(restoreThread_->native_handle(), "OS_ARPSRestore");
}

void RendererPolicyServiceDiedCallback::RestoreTheadLoop()
{
    int32_t tryCounter = 10;
    uint32_t sleepTime = 300000;
    bool restoreResult = false;
    while (!restoreResult && tryCounter > 0) {
        tryCounter--;
        usleep(sleepTime);
        if (renderer_ == nullptr || renderer_->audioStream_ == nullptr || renderer_->abortRestore_) {
            AUDIO_INFO_LOG("abort restore");
            break;
        }
        renderer_->RestoreAudioInLoop(restoreResult, tryCounter);
    }
}

void AudioRendererPrivate::RestoreAudioInLoop(bool &restoreResult, int32_t &tryCounter)
{
    std::lock_guard<std::shared_mutex> lock(rendererMutex_);
    if (IsNoStreamRenderer()) {
        // no stream renderer don't need to restore stream
        restoreResult = audioStream_->RestoreAudioStream(false);
    } else {
        restoreResult = audioStream_->RestoreAudioStream();
        if (!restoreResult) {
            AUDIO_ERR_LOG("restore audio stream failed, %{public}d attempts remaining", tryCounter);
            return;
        }
        abortRestore_ = false;
    }

    InitAudioInterruptCallback();
    if (GetStatusInner() == RENDERER_RUNNING) {
        GetAudioInterrupt(audioInterrupt_);
        int32_t ret = AudioPolicyManager::GetInstance().ActivateAudioInterrupt(audioInterrupt_);
        if (ret != SUCCESS) {
            AUDIO_ERR_LOG("active audio interrupt failed");
        }
    }
    return;
}

int32_t AudioRendererPrivate::SetSpeed(float speed)
{
    AUDIO_INFO_LOG("set speed %{public}f", speed);
    CHECK_AND_RETURN_RET_LOG((speed >= MIN_STREAM_SPEED_LEVEL) && (speed <= MAX_STREAM_SPEED_LEVEL),
        ERR_INVALID_PARAM, "invaild speed index");

    std::lock_guard lock(rendererMutex_);
#ifdef SONIC_ENABLE
    CHECK_AND_RETURN_RET_LOG(audioStream_ != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    audioStream_->SetSpeed(speed);
#endif
    speed_ = speed;
    return SUCCESS;
}

float AudioRendererPrivate::GetSpeed()
{
    std::shared_lock lock(rendererMutex_);
#ifdef SONIC_ENABLE
    CHECK_AND_RETURN_RET_LOG(audioStream_ != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return audioStream_->GetSpeed();
#endif
    return speed_.value_or(1.0f);
}

bool AudioRendererPrivate::IsFastRenderer()
{
    return isFastRenderer_;
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
void AudioRendererPrivate::InitLatencyMeasurement(const AudioStreamParams &audioStreamParams)
{
    latencyMeasEnabled_ = AudioLatencyMeasurement::CheckIfEnabled();
    AUDIO_INFO_LOG("LatencyMeas enabled in renderer:%{public}d", latencyMeasEnabled_);
    if (!latencyMeasEnabled_) {
        return;
    }
    std::string bundleName = AudioSystemManager::GetInstance()->GetSelfBundleName(appInfo_.appUid);
    uint32_t sessionId = 0;
    audioStream_->GetAudioSessionID(sessionId);
    latencyMeasurement_ = std::make_shared<AudioLatencyMeasurement>(audioStreamParams.samplingRate,
        audioStreamParams.channels, audioStreamParams.format, bundleName, sessionId);
}

// Inner function. Must be called with AudioRendererPrivate::rendererMutex_
// or AudioRendererPrivate::streamMutex_ held.
void AudioRendererPrivate::MockPcmData(uint8_t *buffer, size_t bufferSize) const
{
    if (!latencyMeasEnabled_) {
        return;
    }
    if (latencyMeasurement_->MockPcmData(buffer, bufferSize)) {
        std::string timestamp = GetTime();
        audioStream_->UpdateLatencyTimestamp(timestamp, true);
    }
}

void AudioRendererPrivate::ActivateAudioConcurrency(const AudioStreamParams &audioStreamParams,
    const AudioStreamType &streamType, IAudioStream::StreamClass &streamClass)
{
    rendererInfo_.pipeType = PIPE_TYPE_NORMAL_OUT;
    if (rendererInfo_.streamUsage == STREAM_USAGE_VOICE_COMMUNICATION ||
        rendererInfo_.streamUsage == STREAM_USAGE_VOICE_MODEM_COMMUNICATION ||
        rendererInfo_.streamUsage == STREAM_USAGE_VIDEO_COMMUNICATION) {
        rendererInfo_.pipeType = PIPE_TYPE_CALL_OUT;
    } else if (streamClass == IAudioStream::FAST_STREAM) {
        rendererInfo_.pipeType = PIPE_TYPE_LOWLATENCY_OUT;
    } else {
        std::vector<std::shared_ptr<AudioDeviceDescriptor>> deviceDescriptors =
            AudioPolicyManager::GetInstance().GetPreferredOutputDeviceDescriptors(rendererInfo_);
        if (!deviceDescriptors.empty() && deviceDescriptors[0] != nullptr) {
            if ((deviceDescriptors[0]->deviceType_ == DEVICE_TYPE_USB_HEADSET ||
                deviceDescriptors[0]->deviceType_ == DEVICE_TYPE_WIRED_HEADSET) &&
                streamType == STREAM_MUSIC && audioStreamParams.samplingRate >= SAMPLE_RATE_48000 &&
                audioStreamParams.format >= SAMPLE_S24LE) {
                rendererInfo_.pipeType = PIPE_TYPE_DIRECT_MUSIC;
            }
        }
    }
    int32_t ret = AudioPolicyManager::GetInstance().ActivateAudioConcurrency(rendererInfo_.pipeType);
    if (ret != SUCCESS) {
        if (streamClass == IAudioStream::FAST_STREAM) {
            streamClass = IAudioStream::PA_STREAM;
        }
        rendererInfo_.pipeType = PIPE_TYPE_NORMAL_OUT;
    }
    return;
}

void AudioRendererPrivate::ConcedeStream()
{
    AUDIO_INFO_LOG("session %{public}u concede from pipeType %{public}d", sessionID_, rendererInfo_.pipeType);
    uint32_t sessionId = static_cast<uint32_t>(-1);
    int32_t ret = GetAudioStreamId(sessionId);
    CHECK_AND_RETURN_LOG(!ret, "Get sessionId failed");

    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_LOG(currentStream != nullptr, "audioStream_ is nullptr");
    AudioPipeType pipeType = PIPE_TYPE_NORMAL_OUT;
    currentStream->GetAudioPipeType(pipeType);
    rendererInfo_.pipeType = PIPE_TYPE_NORMAL_OUT;
    rendererInfo_.isOffloadAllowed = false;
    currentStream->SetRendererInfo(rendererInfo_);
    switch (pipeType) {
        case PIPE_TYPE_LOWLATENCY_OUT:
            // todo: fix wrong pipe type in server
            CHECK_AND_RETURN_LOG(currentStream->GetStreamClass() != IAudioStream::PA_STREAM,
                "Session %{public}u is pa stream, no need for concede", sessionId);
            [[fallthrough]];
        case PIPE_TYPE_DIRECT_MUSIC:
            SwitchStream(sessionId, IAudioStream::PA_STREAM, AudioStreamDeviceChangeReasonExt::ExtEnum::UNKNOWN);
            break;
        case PIPE_TYPE_OFFLOAD:
            UnsetOffloadMode();
            AudioPolicyManager::GetInstance().MoveToNewPipe(sessionId, PIPE_TYPE_NORMAL_OUT);
            break;
        default:
            break;
    }
}

void AudioRendererPrivate::EnableVoiceModemCommunicationStartStream(bool enable)
{
    isEnableVoiceModemCommunicationStartStream_ = enable;
}

bool AudioRendererPrivate::IsNoStreamRenderer() const
{
    return rendererInfo_.streamUsage == STREAM_USAGE_VOICE_MODEM_COMMUNICATION &&
        !isEnableVoiceModemCommunicationStartStream_;
}

int32_t AudioRendererPrivate::SetDefaultOutputDevice(DeviceType deviceType)
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    if (deviceType != DEVICE_TYPE_EARPIECE && deviceType != DEVICE_TYPE_SPEAKER &&
        deviceType != DEVICE_TYPE_DEFAULT) {
        return ERR_NOT_SUPPORTED;
    }
    bool isSupportedStreamUsage = (find(AUDIO_DEFAULT_OUTPUT_DEVICE_SUPPORTED_STREAM_USAGES.begin(),
        AUDIO_DEFAULT_OUTPUT_DEVICE_SUPPORTED_STREAM_USAGES.end(), rendererInfo_.streamUsage) !=
        AUDIO_DEFAULT_OUTPUT_DEVICE_SUPPORTED_STREAM_USAGES.end());
    CHECK_AND_RETURN_RET_LOG(isSupportedStreamUsage, ERR_NOT_SUPPORTED, "stream usage not supported");
    return currentStream->SetDefaultOutputDevice(deviceType);
}

// diffrence from GetAudioPosition only when set speed
int32_t AudioRendererPrivate::GetAudioTimestampInfo(Timestamp &timestamp, Timestamp::Timestampbase base) const
{
    std::shared_ptr<IAudioStream> currentStream = GetInnerStream();
    CHECK_AND_RETURN_RET_LOG(currentStream != nullptr, ERROR_ILLEGAL_STATE, "audioStream_ is nullptr");
    return currentStream->GetAudioTimestampInfo(timestamp, base);
}
}  // namespace AudioStandard
}  // namespace OHOS
