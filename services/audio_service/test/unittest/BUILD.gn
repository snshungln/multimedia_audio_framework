# Copyright (c) 2022-2025 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//build/test.gni")
import("../../../../config.gni")

module_output_path = "multimedia_audio_framework/audio_balance"

config("module_private_config") {
  visibility = [ ":*" ]

  include_dirs = [
    "../../common/include",
    "../../client/include",
    "../../server/include",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../../../../../foundation/systemabilitymgr/samgr/interfaces/innerkits/samgr_proxy/include",
  ]
}

ohos_unittest("audio_endpoint_separate_unit_test") {
  testonly = true
  module_out_path = module_output_path
  sources = [ "audio_endpoint_separate_unit_test.cpp" ]

  cflags = [ "-fno-access-control" ]

  include_dirs = [
    "../../server/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink/remote_fast",
    "../../../../services/audio_service/server/include",
    "../../../../services/audio_service/server/src",
    "../../../../frameworks/native/audioschedule/include",
    "../../../../frameworks/native/hdiadapter/source/fast",
    "../../../../frameworks/native/hdiadapter/source/common",
    "../../../../frameworks/native/playbackcapturer/include",
  ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:fast_audio_renderer_sink",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service_static",
  ]

  external_deps = [
    "c_utils:utils",
    "drivers_interface_audio:libaudio_proxy_4.0",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = []
  if (audio_framework_feature_low_latency) {
    defines += [ "SUPPORT_LOW_LATENCY" ]
  }
}

ohos_unittest("audio_balance_unit_test") {
  testonly = true
  module_out_path = module_output_path
  cflags = [ "-fno-access-control" ]

  include_dirs = [
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../interfaces/inner_api/native/audiomanager/include",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../frameworks/native/audiopolicy/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/source/common",
    "../../../../services/audio_service/client/src",
    "../../../../services/audio_service/server/include",
    "../../../../frameworks/native/playbackcapturer/include",
  ]
  sources = [ "audio_service_common_unit_test.cpp" ]

  if (audio_framework_feature_low_latency) {
    sources += [
      "audio_balance_unit_test.cpp",
      "audio_service_unit_test.cpp",
    ]
  }

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioeffect:audio_effect",
    "../../../../frameworks/native/audioschedule:audio_schedule",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../services/audio_service:audio_process_service_static",
    "../../../../services/audio_service:audio_service",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
  ]

  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = []
  if (audio_framework_feature_inner_capturer) {
    defines += [ "HAS_FEATURE_INNERCAPTURER" ]
  }
  if (audio_framework_feature_low_latency) {
    defines += [ "SUPPORT_LOW_LATENCY" ]
  }
}

ohos_unittest("audio_direct_sink_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../frameworks/native/audioutils/include",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../interfaces/inner_api/native/audiocommon/include",
  ]

  cflags = [ "-DDEBUG_DIRECT_USE_HDI" ]

  sources = [ "audio_direct_sink_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:audio_renderer_sink",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
    "pulseaudio:pulse",
  ]
}

ohos_unittest("none_mix_engine_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../frameworks/native/audioutils/include",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
  ]

  cflags = [
    "-DDEBUG_DIRECT_USE_HDI",
    "-fno-access-control",
  ]

  sources = [ "none_mix_engine_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:fast_audio_renderer_sink",
    "../../../../frameworks/native/hdiadapter/sink:renderer_sink_adapter",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_process_service_static",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
    "pulseaudio:pulse",
  ]
}

ohos_unittest("audio_thread_task_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../frameworks/native/audioutils/include",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
  ]

  cflags = [
    "-DDEBUG_DIRECT_USE_HDI",
    "-fno-access-control",
  ]

  sources = [ "audio_thread_task_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../services/audio_service:audio_client",
    "../../../../services/audio_service:audio_common",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("audio_service_unit_test") {
  module_out_path = module_output_path

  configs = [ ":module_private_config" ]

  include_dirs = [
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/common/include/limiter",
    "../../../../services/audio_service/common/src",
    "../../../../services/audio_service/common/src/limiter",
    "../../../../services/audio_service/server/include",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../interfaces/inner_api/native/audiomanager/include",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../frameworks/native/audiopolicy/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/source/common",
  ]

  sources = [
    "./client/audio_stream_manager_unit_test.cpp",
    "./client/audio_system_manager_unit_test.cpp",
    "./common/audio_common_converter_unit_test.cpp",
    "./common/audio_down_mix_stereo_unit_test.cpp",
    "./common/audio_volume_unit_test.cpp",
    "./common/limiter_unit_test/audio_limiter_adapter_unit_test.cpp",
    "./common/limiter_unit_test/audio_limiter_manager_unit_test.cpp",
    "./common/limiter_unit_test/audio_limiter_unit_test.cpp",
    "./common/volume_tools_unit_test.cpp",
    "./server/audio_server_unit_test.cpp",
  ]

  if (audio_framework_feature_low_latency) {
    sources += [ "./client/fast_audio_stream_unit_test.cpp" ]
  }

  deps = [
    "../../../../frameworks/native/audioeffect:audio_effect",
    "../../../../frameworks/native/audioschedule:audio_schedule",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../services/audio_service:audio_client",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_service_static",
  ]

  cflags = [
    "-DDEBUG_DIRECT_USE_HDI",
    "-fno-access-control",
  ]

  cflags_cc = [ "-std=c++20" ]

  external_deps = [
    "access_token:libaccesstoken_sdk",
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
  ]

  defines = []
  if (audio_framework_feature_low_latency) {
    defines += [ "SUPPORT_LOW_LATENCY" ]
  }
}

ohos_unittest("pa_renderer_stream_impl_unit_test") {
  testonly = true
  module_out_path = module_output_path
  include_dirs = [
    "../../../../frameworks/native/audiocapturer/include",
    "../../../../services/audio_service/server/include",
  ]
  sources = [ "pa_renderer_stream_impl_unit_test.cpp" ]

  configs = [ ":module_private_config" ]
  cflags = [ "-fno-access-control" ]

  deps = [
    "../../../../frameworks/native/audiocapturer:audio_capturer",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service_static",
  ]
  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("pa_renderer_stream_impl_unit_test_p2") {
  testonly = true
  module_out_path = module_output_path
  include_dirs = [
    "../../../../frameworks/native/audiocapturer/include",
    "../../../../services/audio_service/server/include",
  ]
  sources = [ "pa_renderer_stream_impl_unit_test_p2.cpp" ]

  configs = [ ":module_private_config" ]
  cflags = [ "-fno-access-control" ]

  deps = [
    "../../../../frameworks/native/audiocapturer:audio_capturer",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service_static",
  ]
  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("ipc_stream_in_server_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../frameworks/native/audioutils/include",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
  ]

  sources = [ "ipc_stream_in_server_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:fast_audio_renderer_sink",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_process_service_static",
  ]

  cflags = [ "-fno-access-control" ]

  external_deps = [
    "c_utils:utils",
    "googletest:gmock",
    "hilog:libhilog",
    "pulseaudio:pulse",
  ]
}

ohos_unittest("volume_tools_unit_test") {
  testonly = true
  module_out_path = module_output_path
  include_dirs = [
    "../../../../frameworks/native/audiocapturer/include",
    "../../interfaces/inner_api/native/audiocommon/include",
  ]
  sources = [ "volume_tools_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audiocapturer:audio_capturer",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service",
  ]
  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("audio_process_in_server_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../services/audio_service/server/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/playbackcapturer/include",
    "../../../../interfaces/inner_api/native/audiomanager/include",
    "../../../../frameworks/native/audiopolicy/include",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../frameworks/native/hdiadapter/source/common",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../services/audio_service/client/include",
    "../../../../services/audio_service/common/include",
  ]

  sources = [ "audio_process_in_server_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:fast_audio_renderer_sink",
    "../../../../services/audio_policy:audio_policy_client",
    "../../../../services/audio_service:audio_client",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_process_service_static",
    "../../../../services/audio_service:audio_service",
  ]

  cflags = [ "-fno-access-control" ]

  external_deps = [
    "c_utils:utils",
    "drivers_interface_audio:libeffect_proxy_1.0",
    "googletest:gtest",
    "hdf_core:libhdf_utils",
    "hicollie:libhicollie",
    "hilog:libhilog",
    "ipc:ipc_single",
    "media_foundation:media_monitor_client",
    "media_foundation:media_monitor_common",
    "pulseaudio:pulse",
    "samgr:samgr_proxy",
  ]

  defines = []
  if (audio_framework_feature_low_latency) {
    defines += [ "SUPPORT_LOW_LATENCY" ]
  }
}

ohos_unittest("capturer_in_client_unit_test") {
  module_out_path = module_output_path

  configs = [ ":module_private_config" ]

  include_dirs = [
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../interfaces/inner_api/native/audiomanager/include",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../frameworks/native/audiopolicy/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/source/common",
  ]

  sources = [ "./client/capturer_in_client_unit_test.cpp" ]

  deps = [
    "../../../../frameworks/native/audioeffect:audio_effect",
    "../../../../frameworks/native/audioschedule:audio_schedule",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../services/audio_policy:audio_policy_client",
    "../../../../services/audio_policy:audio_policy_service",
    "../../../../services/audio_service:audio_client",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_process_service",
    "../../../../services/audio_service:audio_service",
  ]

  cflags = [ "-fno-access-control" ]

  external_deps = [
    "ability_base:want",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken_shared",
    "access_token:libprivacy_sdk",
    "access_token:libtokenid_sdk",
    "access_token:libtokensetproc_shared",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "hdf_core:libhdf_ipc_adapter",
    "hdf_core:libhdi",
    "hdf_core:libpub_utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "power_manager:powermgr_client",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("pa_capturer_stream_impl_unit_test") {
  testonly = true
  module_out_path = module_output_path
  include_dirs = [
    "../../../../frameworks/native/audiocapturer/include",
    "../../../../services/audio_service/server/include",
  ]
  sources = [ "pa_capturer_stream_impl_unit_test.cpp" ]

  configs = [ ":module_private_config" ]
  cflags = [ "-fno-access-control" ]

  deps = [
    "../../../../frameworks/native/audiocapturer:audio_capturer",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service_static",
  ]
  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("audio_process_in_client_unit_test") {
  testonly = true
  module_out_path = module_output_path

  include_dirs = [
    "../../../../frameworks/native/audiocapturer/include",
    "../../../../services/audio_service/server/include",
    "../../../../services/audio_service/client/include",
    "../../../../services/audio_service/client/src",
    "../../../../services/audio_service/server/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/playbackcapturer/include",
    "../../../../../../window/window_manager/previewer/mock",
    "../../../../interfaces/inner_api/native/audiomanager/include",
    "../../../../frameworks/native/audiopolicy/include",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../frameworks/native/hdiadapter/source/common",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../services/audio_service/client/include",
    "../../../../services/audio_service/common/include",
  ]

  sources = [ "./client/audio_process_in_client_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  cflags = [ "-fno-access-control" ]

  deps = [
    "../../../../frameworks/native/audiocapturer:audio_capturer",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service_static",
  ]

  external_deps = [
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "googletest:gtest",
    "hicollie:libhicollie",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = []
  if (audio_framework_feature_low_latency) {
    defines += [ "SUPPORT_LOW_LATENCY" ]
  }
}

ohos_unittest("ipc_stream_stub_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../frameworks/native/audioutils/include",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
  ]

  sources = [ "ipc_stream_stub_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:fast_audio_renderer_sink",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_process_service_static",
  ]

  cflags = [ "-fno-access-control" ]

  external_deps = [
    "c_utils:utils",
    "googletest:gmock",
    "hilog:libhilog",
    "pulseaudio:pulse",
  ]
}

ohos_unittest("capturer_in_server_unit_test") {
  testonly = true
  module_out_path = module_output_path
  include_dirs = [ "../../server/include" ]
  sources = [ "capturer_in_server_unit_test.cpp" ]

  configs = [ ":module_private_config" ]
  cflags = [ "-fno-access-control" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_process_service_static",
  ]
  external_deps = [
    "c_utils:utils",
    "drivers_interface_audio:libeffect_proxy_1.0",
    "googletest:gmock",
    "googletest:gtest",
    "hdf_core:libhdf_utils",
    "hicollie:libhicollie",
    "hilog:libhilog",
    "ipc:ipc_single",
    "media_foundation:media_monitor_client",
    "media_foundation:media_monitor_common",
    "pulseaudio:pulse",
    "samgr:samgr_proxy",
  ]

  defines = []
  if (audio_framework_feature_inner_capturer) {
    defines += [ "HAS_FEATURE_INNERCAPTURER" ]
  }
}

ohos_unittest("audio_dump_pcm_unit_test") {
  testonly = true
  module_out_path = module_output_path

  include_dirs = [
    "../../server/include",
    "../../common/include",
    "../../../../frameworks/native/audioschedule/include",
    "../../../../interfaces/inner_api/native/audiocommon/include",
  ]

  sources = [ "audio_dump_pcm_unit_test.cpp" ]

  configs = [ ":module_private_config" ]
  cflags = [ "-fno-access-control" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_common",
  ]

  external_deps = [
    "c_utils:utils",
    "googletest:gmock",
    "pulseaudio:pulse",
  ]
}
